#include "power_control.h"
#include "time.h"
#include "reset.h"
#include "debug.h"
#include "led_driver.h"
#include "flash.h"
#include "memory_layout.h"
#include "input.h"
#include "gpio.h"
#include "timer.h"
#include "cpu.h"
#include "i2c_iface.h"
#include "crc32.h"
#include "trng.h"
#include "crypto.h"
#include "board_info.h"

static i2c_iface_priv_t i2c_iface_priv;

static i2c_slave_t i2c_slave = {
	.cb = i2c_iface_event_cb,
	.priv = &i2c_iface_priv,
};

/** Verify if application image is valid */
static bool verify_application(void)
{
	uint32_t len, crc, res, zero = 0, len_before_csum;
	void *data;
	bool valid;

	data = (void *)APPLICATION_BEGIN;
	len = *(uint32_t *)APPLICATION_CRCSUM;
	crc = *(uint32_t *)(APPLICATION_CRCSUM + 4);

	if (!len || len > APPLICATION_MAX_SIZE || len % 4 ||
	    !crypto_is_valid_app_size(len)) {
		debug("Invalid length stored in application checksum!\n");
		return 0;
	}

	/* do not feed ECDSA signature to crc */
	len -= FIRMWARE_SIGNATURE_SIZE;

	len_before_csum = APPLICATION_CRCSUM - APPLICATION_BEGIN + 4;

	crc32(&res, 0, data, len_before_csum);
	crc32(&res, res, &zero, 4);
	crc32(&res, res, data + len_before_csum + 4,
	      len - (len_before_csum + 4));

	valid = res == crc;
	debug("Application checksum %s\n", valid ? "OK" : "INVALID");

	if (!valid)
		return false;

	if (FIRMWARE_SIGNATURE_SIZE && valid) {
		valid = sys_crypto_verify_application();
		debug("Application signature %s\n", valid ? "OK" : "INVALID");
	}

	return valid;
}

/** Determine if we should boot application firmware or stay in bootloader */
static bool should_boot_application(bool *should_power_on)
{
	reset_reason_info_t info;

	*should_power_on = true;

	switch (get_reset_reason(&info)) {
	case STAY_IN_BOOTLOADER_REQ:
		/* Dpplication jumped to bootloader and requested to stay there
		 * in order to be able to flash new application firmware. Do not
		 * power the board off and on in this case - the operating
		 * system must continue to run in order to flash new firmware.
		 */
		debug("Requested to stay in bootloader\n");
		*should_power_on = false;

		return false;

	case APPLICATION_FAULT:
		debug("Application faulted with fault %#04x, staying in bootloader\n",
		      info.fault);

		return false;

	default:
		return verify_application();
	}
}

__noinline void main(void)
{
	bool power_on;

	/* configure debug console */
	debug_init();

	/* initialize flash peripheral */
	sys_flash_init();

	/* enable CRC32 engine */
	crc32_enable();

	/* configure time counter */
	sys_time_config();

	/* configure LED driver */
	led_driver_config();

	/* Configure SYSRES_OUT to high impedance. Don't change the value! */
	gpio_init_outputs(pin_opendrain, pin_spd_2, 1, SYSRES_OUT_PIN);

	/* enable board cryptography */
	sys_crypto_init();

	debug("Bootloader init complete\n");

	if (should_boot_application(&power_on))
		soft_reset_to_other_program();

	if (power_on) {
		/* configure power control */
		power_control_config();

		/* configure input signals */
		input_signals_config();
	}

	/* enable TRNG */
	sys_trng_init();

	/* initialize board info */
	sys_board_info_init();

	if (power_on)
		/* power the board on */
		power_control_power_on();

	disable_irq();
	i2c_iface_init();
	i2c_slave_init(SLAVE_I2C, &i2c_slave, MCU_I2C_ADDR, 0, 2);
	enable_irq();

	if (power_on)
		/*
		 * After reset selector is selected, will make LEDs blink the
		 * bootloader LED pattern in RED color to indicate that we
		 * failed to boot application.
		 */
		power_control_first_startup();
	else
		/*
		 * Blink LEDs in green color to indicate that we are in
		 * bootloader because the application jumped into it.
		 */
		led_driver_bootloader_pattern_start(GREEN_COLOR);

	sys_input_signals_init();

	/* main loop */
	while (true) {
		if (sys_input_signals_poll()) {
			/* light reset triggered by sysres / manres,
			 * in bootloader we do a hard reset in this case, and we
			 * also disable regulators, otherwise power supply can
			 * causes wrong detection of mmc during boot
			 */
			power_control_set_startup_condition();
			power_control_disable_regulators();
			sys_msleep(100);
			sys_hard_reset();
		}

		i2c_iface_poll();
	}
}

#include "power_control.h"
#include "input.h"
#include "led_driver.h"
#include "i2c_iface.h"
#include "debug.h"
#include "reset.h"
#include "cpu.h"
#include "flash.h"
#include "memory_layout.h"
#include "time.h"
#include "crc32.h"
#include "trng.h"
#include "crypto.h"
#include "board_info.h"

static i2c_iface_priv_t i2c_iface_priv;

static i2c_slave_t i2c_slave = {
	.cb = i2c_iface_event_cb,
	.priv = &i2c_iface_priv,
};

/** Light reset the board */
static void light_reset(void)
{
	debug("Light reset\n");

	disable_irq();
	led_driver_init();
	sys_crypto_reset();
	i2c_iface_init();
	i2c_slave_init(SLAVE_I2C, &i2c_slave, MCU_I2C_ADDR,
		       LED_CONTROLLER_I2C_ADDR, 2);
	enable_irq();

	/* will start knight rider reset pattern after handling reset button */
	power_control_first_startup();

	sys_input_signals_init();
}

__noinline void main(void)
{
	int err;

	/* configure debug console */
	debug_init();

	/* initialize flash peripheral */
	sys_flash_init();

	/* enable CRC32 engine */
	crc32_enable();

	/* configure time counter */
	sys_time_config();

	/* configure power control */
	power_control_config();

	/* configure input signals */
	input_signals_config();

	/* configure LED driver */
	led_driver_config();

	/* enable TRNG */
	sys_trng_init();

	/* enable board cryptography */
	sys_crypto_init();

	/* initialize board info */
	sys_board_info_init();

	debug("\nInit complete, powering the board on...");

	/* power the board on */
	err = power_control_power_on();
	if (err) {
		debug(" failed enabling regulator %d\n", -err);

		/* indicate error of powering up n-th regulator by blinking n-th
		 * LED (from right) in red color for 3 seconds */
		led_set_color24(-err - 1, RED_COLOR);
		led_driver_blink_pattern_start(led_bits(-err - 1), 0, 300, 0,
					       false);
		sys_msleep(3000);
		sys_hard_reset();
	}

	debug(" done\n");

	/* light reset the board */
	light_reset();

	/* main loop */
	while (true) {
		if (sys_input_signals_poll()) {
			/* light reset triggered by sysres / manres */
			light_reset();
			continue;
		}

		if (i2c_iface_poll())
			soft_reset_to_other_program();
	}
}

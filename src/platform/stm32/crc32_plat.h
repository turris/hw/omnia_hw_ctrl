#ifndef CRC32_PLAT_H
#define CRC32_PLAT_H

#include "stm32f0xx.h"
#include "compiler.h"
#include "cpu.h"

#define CRC_FREE_DATA_REG	(CRC->IDR)

uint32_t crc32_plat(uint32_t init, const void *src, uint16_t len);

static inline void crc32_enable(void)
{
	RCC->AHBENR |= RCC_AHBENR_CRCEN;
}

#endif /* CRC32_PLAT_H */

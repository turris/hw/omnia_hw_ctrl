#ifndef TRNG_PLAT_H
#define TRNG_PLAT_H

#include "cpu.h"
#include "stm32f0xx.h"

#ifndef TRNG_PRIV_COMMON_MEMBERS
#error do not include trng_plat.h, it is meant to be included only from trng_common_stm32_gd32.c
#endif

static struct {
	TRNG_PRIV_COMMON_MEMBERS

	uint16_t bits;
	uint8_t word;
} trng;

#define _trng_dma_bits(_b0, _b1) \
	_b0 = ((*_i) & 1), _b1 = ((*(_i + 1)) & 1)

#define trng_for_each_bit_pair(_b0, _b1)				 \
	bool _b0, _b1;							 \
	uint32_t *_i;							 \
	for (_i = &trng.adc_dma_buffer[0], _trng_dma_bits(_b0, _b1);	 \
	     _i < &trng.adc_dma_buffer[ARRAY_SIZE(trng.adc_dma_buffer)]; \
	     _i += 2, _trng_dma_bits(_b0, _b1))

static inline bool trng_bits_same(bool bit0, bool bit1)
{
	return bit0 == bit1;
}

static inline bool trng_entropy_emit_bit(bool bit)
{
	trng.entropy[trng.word] <<= 1;
	trng.entropy[trng.word] |= bit;
	++trng.bits;
	if (trng.bits % 32 == 0)
		++trng.word;

	return bit;
}

static inline bool trng_entropy_full(void)
{
	return trng.bits == 512;
}

static inline void trng_dma_reinit(void)
{
	/* DMA channel needs to be disabled for reinit */
	DMA1_Channel1->CCR &= ~DMA_CCR_EN;
	DMA1_Channel1->CNDTR = ARRAY_SIZE(trng.adc_dma_buffer);
	DMA1_Channel1->CCR |= DMA_CCR_EN;
}

static inline void trng_reset_gathering(void)
{
	trng.word = 0;
	trng.bits = 0;
}

static inline void trng_adc_start(void)
{
	/* start conversion */
	ADC1->CR |= ADC_CR_ADSTART;
}

static inline bool trng_dma_is_error(void)
{
	return DMA1->ISR & DMA_ISR_TEIF1;
}

static inline void trng_dma_irq_clear(void)
{
	DMA1->IFCR = DMA1->ISR;
}

static inline void trng_adc_init(void)
{
	/* enable DMA clock */
	RCC->AHBENR |= RCC_AHBENR_DMAEN;

	/* initialize DMA channel 0 to fill trng.adc_dma_buffer */
	DMA1_Channel1->CPAR = (uint32_t)&ADC1->DR;
	DMA1_Channel1->CMAR = (uint32_t)&trng.adc_dma_buffer;
	DMA1_Channel1->CNDTR = ARRAY_SIZE(trng.adc_dma_buffer);
	DMA1_Channel1->CCR = DMA_CCR_TCIE | DMA_CCR_TEIE | DMA_CCR_MINC |
			     DMA_PeripheralDataSize_Word |
			     DMA_MemoryDataSize_Word | DMA_CCR_EN;

	/* enable DMA irq */
	nvic_enable_irq_with_prio(DMA1_Channel1_IRQn, 3);

	/* reset ADC and enable ADC clock */
	RCC->APB2RSTR |= RCC_APB2RSTR_ADCRST;
	RCC->APB2RSTR &= ~RCC_APB2RSTR_ADCRST;
	RCC->APB2ENR |= RCC_APB2ENR_ADCEN;

	/* configure ADC clock to PCLK / 4 */
	ADC1->CFGR2 = ADC_CFGR2_JITOFFDIV4;

	/* ADC calibration */
	ADC1->CR = ADC_CR_ADCAL;
	while (ADC1->CR & ADC_CR_ADCAL)
		nop();

	/* a little wait is necessary after calibration */
	for (unsigned i = 0; i < 8; ++i)
		nop();

	/* enable ADC */
	ADC1->CR = ADC_CR_ADEN;
	while (!(ADC1->ISR & ADC_ISR_ADRDY))
		nop();

	/* enable temperature sensor */
	ADC->CCR = ADC_CCR_TSEN;

	/* enable temperature sensor channel */
	ADC1->CHSELR = BIT(16);

	/*
	 * enable external trigger on rising edge of timer 1 TRGO output,
	 * enable DMA requests
	 */
	ADC1->CFGR1 = ADC_ExternalTrigConvEdge_Rising |
		      ADC_ExternalTrigConv_T1_TRGO | ADC_CFGR1_DMAEN;

	/* set temperature sensor channel sampling 239.5 clock cycles */
	ADC1->SMPR = 0x7;
}

static inline void trng_adc_disable(void)
{
	/* disable DMA channel */
	DMA1_Channel1->CCR &= ~DMA_CCR_EN;

	/* TODO: need ADSTP ? */

	/* disable ADC */
	ADC1->CR |= ADC_CR_ADDIS;
	while (ADC1->CR & ADC_CR_ADDIS)
		nop();

	/* disable ADC clock and reset ADC */
	RCC->APB2ENR &= ~RCC_APB2ENR_ADCEN;
	RCC->APB2RSTR |= RCC_APB2RSTR_ADCRST;
	RCC->APB2RSTR &= ~RCC_APB2RSTR_ADCRST;

	/* reset DMA channel and IRQs*/
	DMA1->IFCR = DMA1->ISR;
	DMA1_Channel1->CCR = 0;
	DMA1_Channel1->CNDTR = 0;
	DMA1_Channel1->CPAR = 0;
	DMA1_Channel1->CMAR = 0;
	nvic_clear_pending(DMA1_Channel1_IRQn);

	/* disable DMA clock */
	RCC->AHBENR &= ~RCC_AHBENR_DMAEN;
}

#endif /* TRNG_PLAT_H */

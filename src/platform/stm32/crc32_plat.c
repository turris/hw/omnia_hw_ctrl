#include "crc32_plat.h"

__noinline uint32_t crc32_plat(uint32_t init, const void *src, uint16_t len)
{
	CRC->INIT = init;
	CRC->CR = CRC_CR_RESET;

	/*
	 * CRC calculation spends 4 AHB clock cycles for 32 bit data size.
	 * The following code compiles in such a way that there are always at
	 * least 4 clock cycles between CRC_DATA accesses.
	 * But this code must not be optimized more. For example inlining may
	 * optimize away some instructions, and that can result in invalid CRC
	 * calculation.
	 */
	while (len > 0) {
		CRC->DR = get_unaligned32(src);
		src += 4;
		len -= 4;
	}

	return CRC->DR;
}

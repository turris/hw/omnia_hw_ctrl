#ifndef CRYPTO_COMMON_STM32_GD32_H
#define CRYPTO_COMMON_STM32_GD32_H

#include "svc.h"

static __force_inline void crypto_init(void)
{
}
SYSCALL(crypto_init)

static __force_inline void crypto_reset(void)
{
}
SYSCALL(crypto_reset)

#if BOOTLOADER_BUILD
#define FIRMWARE_SIGNATURE_SIZE		0

static inline bool crypto_is_valid_app_size(uint32_t)
{
	return true;
}

static inline bool crypto_verify_application(void)
{
	return true;
}
SYSCALL(crypto_verify_application)
#endif

#endif /* CRYPTO_COMMON_STM32_GD32_H */

#ifndef TRNG_COMMON_STM32_GD32_H
#define TRNG_COMMON_STM32_GD32_H

#include "svc.h"

#if TRNG_ENABLED

void trng_init(void);
SYSCALL(trng_init)

void trng_disable(void);

static __force_inline bool trng_ready(void)
{
	extern bool trng_entropy_is_ready;

	return trng_entropy_is_ready;
}

bool trng_collect_entropy(uint32_t entropy[static 16]);
SYSCALL(trng_collect_entropy, uint32_t *)

#else /* !TRNG_ENABLED */

static __force_inline void trng_init(void)
{
}
SYSCALL(trng_init)

static __force_inline void trng_disable(void)
{
}

static __force_inline bool trng_ready(void)
{
	return false;
}

static __force_inline bool trng_collect_entropy(uint32_t [static 16])
{
	return false;
}
SYSCALL(trng_collect_entropy, uint32_t *)

#endif /* !TRNG_ENABLED */

#endif /* TRNG_COMMON_STM32_GD32_H */

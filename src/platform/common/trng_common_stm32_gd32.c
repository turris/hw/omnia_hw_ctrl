#if TRNG_ENABLED && (defined(STM32F030X8) || defined(GD32F1x0))

#include "cpu.h"
#include "timer.h"
#include "debug.h"

/* trng entropy ready flag */
bool trng_entropy_is_ready;

#define TRNG_PRIV_COMMON_MEMBERS	\
	uint32_t adc_dma_buffer[128];	\
	uint32_t entropy[16];		\
	uint16_t ones, changes;\
	bool prev_bit;

#include "trng_plat.h"

/*
 * We use device internal temperature sensor via ADC as entropy source.
 * The ADC is configured to sample the temperature sensor at 400 Hz.
 * Each time, the sample value is written by DMA to trng.adc_dma_buffer.
 * Once this buffer is full, DMA interrupt is generated. The handler of this
 * interrupt then collets last bit of each sample and filters is with Von
 * Neumann sampling, filling the entropy buffer. Once the entropy buffer is
 * full, the ADC is disabled until the entropy is collected.
 *
 * If the sampled bits have uniform distribution, the Von Neumann sampling
 * generates one bit of entropy from each pair of sampled bits with probability
 * 50%. This means that on average, one bit of entropy is generated every 0.01s,
 * and the entropy buffer, which is 512 bits long, is filled in 5.12s (on
 * average).
 */

static void trng_start(void)
{
	/* start ADC sampling */
	trng_adc_start();

	/* enable ADC trigger timer */
	timer_enable(TRNG_ADC_TIMER, true);
}

void trng_init(void)
{
	/* initalize DMA and ADC */
	trng_adc_init();

	/* reset gathering variables */
	trng_entropy_is_ready = false;
	trng_reset_gathering();

	/* initialize ADC trigger timer to generate trigger events at 400 Hz */
	timer_init_trigger(TRNG_ADC_TIMER, 400);

	/* start gathering */
	trng_start();
}

void trng_disable(void)
{
	/* disable ADC trigger timer */
	timer_enable(TRNG_ADC_TIMER, false);

	/* disable DMA and ADC */
	trng_adc_disable();
}

static inline void trng_randomness_test_add_bit(bool bit)
{
	trng.ones += bit;
	trng.changes += trng.prev_bit ^ bit;
	trng.prev_bit = bit;
}

static inline bool trng_randomness_test_passed(void)
{
	bool res;

	/*
	 * The number of ones in a random binary sequence follows a binomial
	 * distribution with p=0.5, assuming a fair distribution of 0s and 1s
	 * In that case the expected count of 1s is 512 / 2 = 256.
	 *
	 * We reject a given sequence if there are too many or too few 1s, i.e.
	 * if |256 - #1s| is too big. We also do this test for the number of
	 * changes in the binary sequence.
	 *
	 * Taking 60 as the bound will reject approximately one sequence per
	 * year, on average.
	 */
	res = ABSDIFF(256, trng.ones) <= 60 &&
	      ABSDIFF(256, trng.changes) <= 60;

	if (0)
		debug("1s = %d (%d), 's = %d (%d)  ---  %s\n",
		      trng.ones, ABSDIFF(256, trng.ones),
		      trng.changes, ABSDIFF(256, trng.changes),
		      res ? "OK" : "FAIL");

	/* reset randomness test variables */
	trng.ones = trng.changes = 0;

	return res;
}

void __noinline trng_dma_irq_handler(void)
{
	if (trng_dma_is_error()) {
		debug("TRNG DMA error interrupt\n");

		/* re-initialize */
		trng_disable();
		trng_init();

		return;
	}

	/* clear interrupt flags */
	trng_dma_irq_clear();

	/* reinitialize DMA count */
	trng_dma_reinit();

	/*
	 * We use Von Neumann sampling: in each pair of bits, if the bits are
	 * different, emit one bit of output (10 -> 1, 01 -> 0); if they are
	 * same, ignore the pair.
	 */
	trng_for_each_bit_pair(bit0, bit1) {
		bool bit;

		if (trng_bits_same(bit0, bit1))
			continue;

		/* the bits are different, emit the first bit */
		bit = trng_entropy_emit_bit(bit0);

		/* add the bit to randomness test */
		trng_randomness_test_add_bit(bit);

		/* stop if entropy buffer is full */
		if (trng_entropy_full())
			break;
	}

	/* if entropy buffer is full */
	if (trng_entropy_full()) {
		/* reset entropy filling for next gathering */
		trng_reset_gathering();

		/*
		 * discard the collected entropy if it does not pass randomness
		 * test, start gathering again
		 */
		if (!trng_randomness_test_passed()) {
			trng_adc_start();
			return;
		}

		/* disable ADC trigger timer to save power */
		timer_enable(TRNG_ADC_TIMER, false);

		/* set ready flag */
		trng_entropy_is_ready = true;
	} else {
		/* start ADC sampling to continue gathering */
		trng_adc_start();
	}
}

bool trng_collect_entropy(uint32_t entropy[static 16])
{
	if (!trng_entropy_is_ready)
		return false;

	for (unsigned i = 0; i < ARRAY_SIZE(trng.entropy); ++i)
		entropy[i] = trng.entropy[i];

	/* unset ready flag */
	trng_entropy_is_ready = false;

	/* start gathering again */
	trng_start();

	return true;
}

#endif /* TRNG_ENABLED && (defined(STM32F030X8) || defined(GD32F1x0)) */

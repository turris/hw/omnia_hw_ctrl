#if PRIVILEGES

#include "cpu.h"
#include "svc.h"
#include "mpu.h"
#include "input.h"
#include "signal.h"
#include "led_driver.h"
#include "power_control.h"
#include "clock.h"
#include "i2c_slave.h"
#include "irq.h"
#include "time.h"
#include "led_driver.h"
#include "flash.h"
#include "firmware_flash.h"
#include "reset.h"
#include "timer.h"
#include "watchdog.h"
#include "poweroff.h"
#include "debug.h"
#include "trng.h"
#include "crypto.h"
#include "board_info.h"

#pragma GCC optimize ("O3")

void hardfault_handler(void);

void __irq svc_handler(void)
{
	exception_frame_t *frame = (void *)get_psp();
	svc_t svc = *(uint8_t *)(frame->pc - 2);
	uint32_t arg1 = frame->r0;
	uint32_t arg2 = frame->r1;
	uint32_t arg3 = frame->r2;

	/* input_signals_poll() is the most frequent syscall */
	if (likely(svc == SYS_input_signals_poll)) {
		frame->r0 = input_signals_poll();
	} else switch (svc) {
	case SYS_sigreturn:
		sigreturn();
		break;
	case SYS_button_counter_decrease:
		button_counter_decrease(arg1);
		break;
	case SYS_button_set_user_mode:
		button_set_user_mode(arg1);
		break;
	case SYS_input_signals_init:
		input_signals_init();
		break;
	case SYS_power_control_usb:
		if (arg1 != USB3_PORT0 && arg1 != USB3_PORT1) {
			debug("sys_power_control_usb: invalid port %u\n", arg1);
			hardfault_handler();
		}
		power_control_usb(arg1, arg2, arg3);
		break;
	case SYS_led_driver_set_brightness:
		led_driver_set_brightness(arg1);
		break;
	case SYS_led_driver_overwrite_brightness:
		led_driver_overwrite_brightness(arg1, arg2);
		break;
	case SYS_led_set_user_mode:
		led_set_user_mode(arg1, arg2);
		break;
	case SYS_led_set_state_user:
		led_set_state_user(arg1, arg2);
		break;
	case SYS_clk_config:
		clk_config(arg1, arg2);
		break;
	case SYS_enable_irq_with_prio:
		enable_irq_with_prio(arg1, arg2);
		break;
	case SYS_time_config:
		time_config();
		break;
	case SYS_msleep:
		msleep(arg1);
		break;
	case SYS_watchdog_enable:
		watchdog_enable(arg1);
		break;
	case SYS_watchdog_set_timeout:
		watchdog_set_timeout(arg1);
		break;
	case SYS_flash_init:
		flash_init();
		break;
#if !FORBID_FIRMWARE_FLASH
	case SYS_plat_firmware_flash_finish:
		plat_firmware_flash_finish((void *)arg1, (void *)arg2);
		break;
#endif
	case SYS_plat_soft_reset_to_other_program:
		plat_soft_reset_to_other_program();
		break;
	case SYS_hard_reset:
		hard_reset();
		break;
	case SYS_poweroff:
		poweroff(arg1, arg2);
		break;
	case SYS_trng_init:
		trng_init();
		break;
	case SYS_trng_collect_entropy:
		assert_unprivileged_can_write((void *)arg1, 64,
					      "sys_trng_collect_entropy");
		frame->r0 = trng_collect_entropy((uint32_t *)arg1);
		break;
	case SYS_crypto_init:
		crypto_init();
		break;
	case SYS_crypto_reset:
		crypto_reset();
		break;
#if BOOTLOADER_BUILD
	case SYS_crypto_verify_application:
		frame->r0 = crypto_verify_application();
		break;
#else /* !BOOTLOADER_BUILD */
	case SYS_crypto_sign_message:
		assert_unprivileged_can_read((void *)arg1, SHA256_CTX_SIZE,
					     "sys_crypto_sign_message");
		frame->r0 = crypto_sign_message((void *)arg1);
		break;
	case SYS_crypto_signature_collect:
		assert_unprivileged_can_write((void *)arg1,
					      2 * PKHA_NUMBER_SIZE,
					      "sys_crypto_signature_collect");
		frame->r0 = crypto_signature_collect((void *)arg1);
		break;
	case SYS_crypto_get_public_key:
		assert_unprivileged_can_write((void *)arg1,
					      1 + PKHA_NUMBER_SIZE,
					      "sys_crypto_get_public_key");
		frame->r0 = crypto_get_public_key((void *)arg1);
		break;
#endif /* !BOOTLOADER_BUILD */
	case SYS_board_info_init:
		board_info_init();
		break;
	case SYS_board_info_get:
		assert_unprivileged_can_write((void *)arg1,
					      sizeof(board_info_t),
					      "sys_board_info_get");
		frame->r0 = board_info_get((void *)arg1);
		break;
	case SYS_board_info_burn:
		assert_unprivileged_can_read((void *)arg1,
					     sizeof(board_info_t),
					     "sys_board_info_get");
		frame->r0 = board_info_burn((void *)arg1);
		break;
	default:
		debug("unhandled svc(%u, %#10x, %#10x)\n", svc, arg1, arg2);
		break;
	}
}

#endif /* PRIVILEGES */

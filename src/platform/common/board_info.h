#ifndef BOARD_INFO_COMMON_H
#define BOARD_INFO_COMMON_H

#include "svc.h"

#ifndef BOARD_INFO_ENABLED
#error build system did not define BOARD_INFO_ENABLED macro
#endif

typedef struct {
	uint32_t serial_lo; /* board serial number, low word */
	uint32_t serial_hi; /* board serial number, high word */
	uint8_t mac[6]; /* board first MAC address */
	uint8_t revision; /* board revision */

	/* checksum, low 8 bits of crc32 of this structure
	 * (with csum field zeroed when computing csum)
	 */
	uint8_t csum;
} board_info_t __attribute__((__aligned__(4)));

#if BOARD_INFO_ENABLED

/** Initialize board info
 * Reads board info structure from non-volatile memory
 */
void board_info_init(void);

/** Fill in board info structure with board information that was previously read
 * with call to board_info_init().
 * @param [out] info pointer to board info structure that will be filled in
 * @returns true on success, false if there is no valid board info
 */
bool board_info_get(board_info_t *info);

/** Burn board info, if not yet burned.
 * @param info pointer to board information that should be burned
 * @returns true on success, false if burning failed for whatever reason
 */
bool board_info_burn(const board_info_t *info);

#else /* !BOARD_INFO_ENABLED */

static inline void board_info_init(void)
{
}
static inline bool board_info_get(board_info_t *)
{
	return false;
}
static inline bool board_info_burn(const board_info_t *)
{
	return false;
}

#endif /* BOARD_INFO_ENABLED */

SYSCALL(board_info_init)
SYSCALL(board_info_get, board_info_t *)
SYSCALL(board_info_burn, const board_info_t *)

#endif /* BOARD_INFO_COMMON_H */

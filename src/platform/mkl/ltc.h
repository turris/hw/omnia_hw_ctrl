#ifndef LTC_H
#define LTC_H

#include "cpu.h"
#include "debug.h"

typedef void (*ltc_callback_t)(void *result);

/** Initialize the LTC peripheral.
 * Enables LTC clock, configures DMA and LTC interrupt.
 */
void ltc_init(void);

/** Reset LTC state and private data, callback with failure indicator. */
void ltc_reset(void);

#define SHA256_CTX_SIZE		32

/** Asynchornous SHA256 hash computation.
 * Starts computation of SHA256 of @ref data of length @ref length.
 * When the computation is done, the result is stored into @ref hash, and
 * the callback function @ref callback is called, with the @ref hash pointer
 * as parameter.
 * Only one SHA256 computation may be running at a given time.
 * @returns true if started succesfully, false if LTC is busy
 */
bool ltc_sha256_async(void *hash, const void *data,
		      uint16_t length, ltc_callback_t callback);

/** Synchronous SHA256 hash computation.
 * Synchronously computes SHA256 of @ref data of length @ref length and stores
 * the result into @ref hash.
 * @returns true if computation succesful, false if LTC is busy
 */
bool ltc_sha256(void *hash, const void *data, uint16_t length);

#define PKHA_NUMBER_SIZE	32
#define PKHA_NUMBER_WORDS	(PKHA_NUMBER_SIZE / 4)

/* Type encapsulating long numbers used in ECDSA. */
typedef union {
	uint32_t words[PKHA_NUMBER_WORDS];
} pkha_number_t;

typedef struct {
	pkha_number_t x; /* x coordinate */
	pkha_number_t y; /* y coordinate */
} pkha_point_t;

typedef struct {
	pkha_point_t G; /* curve base point */
	pkha_number_t a; /* a parameter of curve */
	pkha_number_t b; /* b parameter of curve */
	pkha_number_t n; /* n - order of [Gx, Gy] - modulus for integer operations */
	pkha_number_t p; /* p - prime parameter of curve - modulus for curve operations */
} pkha_curve_t;

typedef struct {
	pkha_number_t r; /* first part of ECDSA signature */
	pkha_number_t s; /* second part of ECDSA signature */
} pkha_signature_t;

/** Store PKHA number in big endian order to unaligned destination
 * @param dst the destination pointer where the number will be stored in big
 *        endian order; need not be aligned
 * @param src the number to store
 * @returns pointer just after the end of the stored number
 */
void *pkha_number_store_be(void *dst, const pkha_number_t *src);

/** Load PKHA number from unaligned destination in big endian order
 * @param dst pointer to destination PKHA number
 * @param src the pointer to the source where the number is is big endian
 *        order; need not be aligned
 */
void pkha_number_load_be(pkha_number_t *dst, const void *src);

/** Compare two PKHA numbers
 * @param a the first number
 * @param b the second number
 * @returns -1 if a < b, 0 if a == b, 1 if a > b
 */
int pkha_number_cmp(const pkha_number_t *a, const pkha_number_t *b);

/** Check whether a PKHA number is zero
 * @param x the PKHA number to compare to zero
 * @returns true if @ref x is zero, false otherwise
 */
bool pkha_number_is_zero(const pkha_number_t *x);

#if DBG_ENABLE
/** Dumps PKHA number
 * @param x the PKHA number to dump
 * @param what the name of the number, will be printed before the number
 */
void pkha_number_dump(const pkha_number_t *x, const char *what);
#else
static inline void pkha_number_dump(const pkha_number_t *, const char *)
{
}
#endif

/** Asynchronous ECDSA signature generation
 * @param curve the definition of elliptic curve over a prime field
 * @param priv_key private key with which to sign @ref msg
 * @param msg SHA256 digest of a message which to sign
 * @param callback function which will be called when the computation is done
 * @param result pointer where the signature will be stored
 * @returns true if started succesfully, false if LTC is busy
 */
bool ltc_ecdsa_sign(const pkha_curve_t *curve, const pkha_number_t *priv_key,
		    const void *msg, ltc_callback_t callback,
		    pkha_signature_t *result);

/** Asynchronous ECDSA signature verification
 * @param curve the definition of elliptic curve over a prime field
 * @param pub_key public key against which to verify that the signature @ref
 *        signature is a valid signature of @ref msg
 * @param msg SHA256 digest of a message that @ref signature is a signature of
 * @param callback function which will be called when the computation is done
 * @param result pointer where the result of the verification will be stored
 * @returns true if started succesfully, false if LTC is busy
 */
bool ltc_ecdsa_verify(const pkha_curve_t *curve, const pkha_point_t *pub_key,
		      const void *msg, const pkha_signature_t *signature,
		      ltc_callback_t callback, bool *result);

/** Synchronous ECDSA public key computation (compressed form)
 * @param curve the definition of elliptic curve over a prime field
 * @param priv_key private key for which we want to compute the the public key
 * @param [out] result pointer where the computed compressed public key will be
 *        stored
 * @returns true if computed succesfully, false if LTC is busy
 */
bool ltc_ecdsa_public_key_compressed_sync(const pkha_curve_t *curve,
					  const pkha_number_t *priv_key,
					  uint8_t result[static 1 + PKHA_NUMBER_SIZE]);

#endif /* LTC_H */

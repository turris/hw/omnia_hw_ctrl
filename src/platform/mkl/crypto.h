#ifndef CRYPTO_H
#define CRYPTO_H

#include "svc.h"
#include "ltc.h"

/** Initialize crypto syscall. Initializes the underlying peripheral,
 * runs unit tests (if enabled), and initializes board ECDSA key.
 * In BOOTLOADER_BUILD also secures the flash.
 */
void crypto_init(void);
SYSCALL(crypto_init)

/** Reset crypto syscall. If there are any crypto operations in progress,
 * abort them.
 */
void crypto_reset(void);
SYSCALL(crypto_reset)

/* Firmware verification callback type */
typedef void (*verify_callback_t)(bool success, void *priv);

/** Verify firmware signature
 * Verifies the firmware cryptographic signature. Should be used before
 * flashing the firmware.
 * @param firmware the pointer to the firmware blob
 * @param size the length of the firmware
 * @param callback the callback to be called when verification is done
 * @param priv the cookie to be passed to the callback
 */
void crypto_verify_firmware_signature(const void *firmware, uint16_t size,
				      verify_callback_t callback, void *priv);

/* The structure that resides at the end of the firmware binary if the
 * firmware is signed.
 */
typedef struct {
	uint8_t hash[SHA256_CTX_SIZE]; /* sha256 hash of the firmware */
	pkha_signature_t signature; /* ECDSA signature of the hash */
} firmware_signature_t;

#define FIRMWARE_SIGNATURE_SIZE		sizeof(firmware_signature_t)

#if BOOTLOADER_BUILD

static inline bool crypto_is_valid_app_size(uint32_t size)
{
	return size >= FIRMWARE_SIGNATURE_SIZE;
}

/** Verify the signature of the application firmware */
bool crypto_verify_application(void);
SYSCALL(crypto_verify_application)

#else /* !BOOTLOADER_BUILD */

/** Asynchronously sign a message with board ECDSA private key
 * @param msg SHA256 digest of a message to sign
 * @returns true if signature compuation started successfuly, false if busy
 */
bool crypto_sign_message(const uint8_t msg[static SHA256_CTX_SIZE]);
SYSCALL(crypto_sign_message, const uint8_t *)

/** Checks whether signing is done */
bool crypto_signature_done(void);

/** Collects the signature
 * @param [out] signature pointer where the signature will be stored
 * @returns true if collected, false if there is no signature to be collected
 */
bool crypto_signature_collect(uint8_t signature[static 2 * PKHA_NUMBER_SIZE]);
SYSCALL(crypto_signature_collect, uint8_t *)

/** Get board public key
 * @param [out] public_key pointer where the public key will be stored
 * @returns true if board has a valid ECDSA key, false otherwise (in this case
 *          nothing is stored into @ref public_key
 */
bool crypto_get_public_key(uint8_t public_key[static 1 + PKHA_NUMBER_SIZE]);
SYSCALL(crypto_get_public_key, uint8_t *)

#endif /* !BOOTLOADER_BUILD */

#endif /* CRYPTO_H */

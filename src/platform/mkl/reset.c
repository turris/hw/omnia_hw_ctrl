#include "cpu.h"
#include "reset.h"
#include "memory_layout.h"
#include "trng.h"
#include "dma.h"

static __privileged __noinline void _do_reset_to_other_program(void)
{
	/* Stop TRNG */
	trng_stop();

	/* Reset LTC if it is enabled */
	if (BME_BITFIELD(SIM_SCGC5, SIM_SCGC5_LTC))
		LTC_COM = LTC_COM_ALL;

	/* Reset DMA */
	dma_reset();

	/* Set MPU region 0 access to default values */
	MPU_RGDAACn(0) = MPU_RGDn_WORD2_MnSM_AS_UM(0) |
			 MPU_RGDn_WORD2_MnUM_RWX(0) |
			 MPU_RGDn_WORD2_MnSM_AS_UM(2) |
			 MPU_RGDn_WORD2_MnUM_RWX(2);

	/* Disable other MPU regions */
	for (unsigned reg = 1; reg < 8; ++reg)
		MPU_RGDn_WORD3(reg) = 0;

	if (!BOOTLOADER_BUILD)
		set_reset_reason(STAY_IN_BOOTLOADER_REQ, 0);

	reset_to_address(BOOTLOADER_BUILD ? APPLICATION_BEGIN
					  : BOOTLOADER_BEGIN);
}

static void __naked __privileged do_reset_to_other_program(void)
{
	/* Use main stack pointer */
	set_control(0);

	_do_reset_to_other_program();
}

__privileged __noreturn void plat_soft_reset_to_other_program(void)
{
	volatile exception_frame_t *frame;

	disable_irq();

	/*
	 * Update exception frame so that it returns to the
	 * do_reset_to_other_program function.
	 */
	frame = (exception_frame_t *)get_psp();
	frame->psr = 0x1000000;
	frame->pc = (uint32_t)do_reset_to_other_program | 0x1;

	/* Set thread context to privileged mode */
	set_control(CONTROL_SPSEL);

	/* Immediately return from SVC exception */
	svc_return();
}

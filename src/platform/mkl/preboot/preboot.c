/*
 * MKL firmware has a pre-bootloader in the first flash page.
 *
 * This is because we may want to support upgrading bootloader firmware, but
 * that would entail erasing pages. We want to avoid having to erase the first
 * page, because it contains the Flash Configuration Field (FCF). Erasing FCF
 * would unlock unlock flash security if the flash is locked.
 *
 * Thus the first flash page contains the configuration area and a small code
 * that jumps to the real bootloader firmware.
 */

#include "cpu.h"
#include "memory_layout.h"
#include "reset_common.h"

extern uint32_t _stack_top;

void __noreturn __naked reset_handler(void)
{
	reset_to_address(BOOTLOADER_BEGIN);
}

static void __irq __naked default_handler(void)
{
	disable_irq();

	while (true)
		wait_for_interrupt();
}

void nmi_handler(void) __weak_alias(default_handler);
void hardfault_handler(void) __weak_alias(default_handler);
void svc_handler(void) __weak_alias(default_handler);
void pendsv_handler(void) __weak_alias(default_handler);
void systick_irq_handler(void) __weak_alias(default_handler);

static __used __section(".isr_vector") void * const isr_vector[] = {
	&_stack_top,
	reset_handler,
	nmi_handler,
	hardfault_handler,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	svc_handler,
	NULL,
	NULL,
	pendsv_handler,
	systick_irq_handler,
	default_handler,	/* DMA0 channel 0 or 4 transfer complete */
	default_handler,	/* DMA0 channel 1 or 5 transfer complete */
	default_handler,	/* DMA0 channel 2 or 6 transfer complete */
	default_handler,	/* DMA0 channel 3 or 7 transfer complete */
	default_handler,	/* DMA0 error interrupt */
	default_handler,	/* Flexible IO */
	default_handler,	/* Timer/PWM module 0 */
	default_handler,	/* Timer/PWM module 1 */
	default_handler,	/* Timer/PWM module 2 */
	default_handler,	/* Periodic interrupt timer 0 */
	default_handler,	/* Serial peripheral interface 0 */
	default_handler,	/* EMVSIM0 */
	default_handler,	/* Low power UART 0 */
	default_handler,	/* Low power UART 1 */
	default_handler,	/* I2C module 0 */
	default_handler,	/* QSPI0 */
	default_handler,	/* DryIce tamper */
	default_handler,	/* Port A */
	default_handler,	/* Port B */
	default_handler,	/* Port C */
	default_handler,	/* Port D */
	default_handler,	/* Port E */
	default_handler,	/* Low leakage wake up */
	default_handler,	/* Low power trusted cryptographic 0 */
	default_handler,	/* Universal serial bus 0 */
	default_handler,	/* Analog to digital convertor 0 */
	default_handler,	/* Low power timer 0 */
	default_handler,	/* Real time clock seconds */
	default_handler,	/* Selectable peripheral interrupt INTMUX0-0 */
	default_handler,	/* Selectable peripheral interrupt INTMUX0-1 */
	default_handler,	/* Selectable peripheral interrupt INTMUX0-2 */
	default_handler,	/* Selectable peripheral interrupt INTMUX0-3 */
};

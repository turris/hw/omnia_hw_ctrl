#ifndef __FLASH_PLAT_H
#define __FLASH_PLAT_H

#include "flash.h"
#include "cpu.h"
#include "debug.h"

static __privileged inline void flash_plat_init(void)
{
	/* Clear all FTFA flags */
	FTFA_FSTAT = FTFA_FSTAT_FPVIOL | FTFA_FSTAT_ACCERR |
		     FTFA_FSTAT_RDCOLERR;

	/* enable INTMUX clock */
	BME_OR(SIM_SCGC6) = SIM_SCGC6_INTMUX0;

	/* reset INTMUX channel 0 and mux FTFA IRQ */
	INTMUX_CHn_CSR(0) = INTMUX_CHn_CSR_RST;
	INTMUX_CHn_IER(0) = BIT(FTFA_IRQn);

	/* Enable INTMUX channel 0 interrupt. Flash irq MUST NOT PREEMPT SVC,
	 * since it can enqueue signals. See the IMPORTANT note in signal.h.
	 */
	nvic_enable_irq_with_prio(INTMUX0_0_IRQn, 3);
}

static __privileged inline uint32_t flash_plat_get_and_clear_status(void)
{
	uint32_t st = FTFA_FSTAT;

	FTFA_FSTAT = st & (FTFA_FSTAT_FPVIOL | FTFA_FSTAT_ACCERR);

	return st;
}

static __privileged inline bool flash_plat_status_okay(uint32_t stat)
{
	return (stat & (FTFA_FSTAT_CCIF | FTFA_FSTAT_MGSTAT0)) ==
	       FTFA_FSTAT_CCIF;
}

static __privileged inline bool flash_plat_status_error(uint32_t stat)
{
	return stat & FTFA_FSTAT_MGSTAT0;
}

static __privileged inline void flash_plat_op_begin(flash_op_type_t)
{
	/* Clear all FTFA flags */
	FTFA_FSTAT = FTFA_FSTAT_FPVIOL | FTFA_FSTAT_ACCERR |
		     FTFA_FSTAT_RDCOLERR;
}

static __privileged inline void flash_plat_op_end(flash_op_type_t)
{
	/* disable command completed interrupt */
	BME_AND(FTFA_FCNFG) = (uint8_t)~FTFA_FCNFG_CCIE;

	/*
	 * clear flash controller cache
	 * (don't use BME_OR, since we can't use BME for MCM access)
	 */
	MCM_PLACR |= MCM_PLACR_CFCC;
}

static __privileged inline void flash_plat_set_cmd_addr(uint8_t cmd,
							uint32_t addr)
{
	FTFA_FCCOB_LWORD(0) = (cmd << 24) | (addr & 0xffffff);
}

static __privileged inline void flash_plat_cmd_exec(void)
{
	/* start operation */
	FTFA_FSTAT = FTFA_FSTAT_CCIF;

	/* enable command completed interrupt */
	BME_OR(FTFA_FCNFG) = FTFA_FCNFG_CCIE;
}

static __privileged inline void flash_plat_erase_next(uint32_t addr)
{
	/* set erase command and address */
	flash_plat_set_cmd_addr(FTFA_FCCOB_FCMD_ERS_SECT, addr);

	/* execute the command */
	flash_plat_cmd_exec();
}

static __privileged inline void flash_plat_write_next(uint32_t *addr,
						      const uint8_t **src)
{
	/* set address */
	flash_plat_set_cmd_addr(FTFA_FCCOB_FCMD_PRG_LWRD, *addr);
	*addr += 4;

	/* set data */
	FTFA_FCCOB_LWORD(1) = get_unaligned32(*src);
	*src += 4;

	/* execute the command */
	flash_plat_cmd_exec();
}

/* The following functions are MKL specific */

/** Set flash Read/Program Once command and index
 * @param cmd command code for Read Once or Program Once operation
 * @param idx the index of the Program Once Field
 */
static __privileged inline void
flash_set_cmd_once_idx(uint8_t cmd, uint8_t idx)
{
	FTFA_FCCOB(0) = cmd;
	FTFA_FCCOB(1) = idx;
}

/** Synchronously execute flash operation
 * @returns true if operation was successful, false if an error occured
 */
static __privileged inline bool flash_cmd_exec_sync(void)
{
	uint8_t st;

	/* start operation */
	FTFA_FSTAT = FTFA_FSTAT_CCIF;

	/* busy wait for operation to finish */
	while (!BME_BITFIELD(FTFA_FSTAT, FTFA_FSTAT_CCIF))
		nop();

	/* check status */
	st = FTFA_FSTAT & (FTFA_FSTAT_ACCERR | FTFA_FSTAT_FPVIOL);
	if (st) {
		/* clear ACCERR or FPVIOL */
		FTFA_FSTAT = st;

		debug("flash cmd failed:%s%s\n",
		      (st & FTFA_FSTAT_ACCERR) ? " ACCERR" : "",
		      (st & FTFA_FSTAT_FPVIOL) ? " FPVIOL" : "");

		return false;
	}

	return true;
}

/** Synchronously read one record in Program Once Field of flash IFR
 * @param idx the index of the field, can be 0x0 - 0x09, 0x30 - 0x33
 * @returns the read value, or 0xffffffff on failure; 0xffffffff is also
 *          returned if the field is not programmed yet
 */
static __privileged inline uint32_t flash_read_once_sync(uint8_t idx)
{
	flash_set_cmd_once_idx(FTFA_FCCOB_FCMD_RD_ONCE, idx);

	if (!flash_cmd_exec_sync()) {
		debug("could not read flash IFR index %#04x\n", idx);

		return 0xffffffff;
	}

	return FTFA_FCCOB_LWORD(1);
}

/** Synchronously program one record in Program Once Field of flash IFR
 * @param idx the index of the field, can be 0x0 - 0x09, 0x20 - 0x23,
 *        0x30 - 0x33
 * @param val the value to program into the field
 * @returns true if successful, false if an error occured
 */
static __privileged inline bool
flash_program_once_sync(uint8_t idx, uint32_t val)
{
	flash_set_cmd_once_idx(FTFA_FCCOB_FCMD_PRG_ONCE, idx);

	FTFA_FCCOB_LWORD(1) = val;

	if (!flash_cmd_exec_sync()) {
		debug("could not program flash IFR index %#04x\n", idx);
		return false;
	}

	return true;
}

/** Synchronously program one byte of flash
 * @param addr address in the flash
 * @param val the new byte value
 * @return true on success, false on failure
 */
static __privileged inline bool
flash_program_byte_sync(uint32_t addr, uint8_t val)
{
	uint32_t word_addr, word, word_new;
	uint8_t offset;

	/* get word address of the byte, and offset in the word */
	word_addr = addr & ~0x3;
	offset = (addr & 0x3) << 3;

	/* get old word value */
	word = *(volatile const uint32_t *)word_addr;

	/* replace new byte value into the word */
	word_new = (word & ~(0xffU << offset)) | (val << offset);

	/* if there is no change, exit */
	if (word == word_new)
		return true;

	/* if some bits are being changed from 0 to 1, fail */
	if (word_new & ~word)
		return false;

	/* set flash command, address and data */
	flash_plat_set_cmd_addr(FTFA_FCCOB_FCMD_PRG_LWRD, word_addr);
	FTFA_FCCOB_LWORD(1) = word_new;

	/* synchronously execute the command */
	return flash_cmd_exec_sync();
}

/** Check whether flash is in secure mode
 * @returns true if flash is in secure mode, false if it is not
 */
static __privileged inline bool flash_is_secure(void)
{
	return (FTFA_FSEC & FSEC_SEC_MASK) != FSEC_SEC_UNSECURE;
}

/** Synchronously set flash to secure mode
 * @returns true if successful, false otherwise
 */
static __privileged inline bool flash_set_secure_sync(void)
{
	return flash_program_byte_sync((uint32_t)&FCF_FSEC,
				       (FCF_FSEC & ~FSEC_SEC_MASK) |
				       FSEC_SEC_SECURE);
}

#endif /* __FLASH_PLAT_H */

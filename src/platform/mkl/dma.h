#ifndef DMA_H
#define DMA_H

#include "cpu.h"

#define DMACHAN_LTC_IFIFO	0

typedef struct {
	uint32_t saddr;
	uint16_t soff;
	uint16_t attr;
	uint32_t nbytes;
	int32_t slast;
	uint32_t daddr;
	uint16_t doff;
	uint16_t citer;
	int32_t dlastsga;
	uint16_t csr;
	uint16_t biter;
} dma_tcd_t;

static __privileged inline void dma_reset(void)
{
	/* disable all hwreqs */
	DMA_CERQ = DMA_CERQ_CAER;

	/* cancel current channel and disallow the start of any new channel */
	BME_OR(DMA_CR) = DMA_CR_CX | DMA_CR_HALT;

	/* wait for active bit to clear */
	while (BME_BITFIELD(DMA_CR, DMA_CR_ACTIVE))
		nop();

	/* clear all EEI bits */
	DMA_CEEI = DMA_CEEI_CAEE;

	/* clear all INT bits */
	DMA_CINT = DMA_CINT_CAIR;

	/* clear all ERR bits */
	DMA_CERR = DMA_CERR_CAEI;

	/* clear all EARS bits */
	DMA_EARS = 0;

	for (unsigned chn = 0; chn < 8; ++chn) {
		/* set default channel priority */
		DMA_DCHPRI(chn) = chn;

		/* reset channel CSR (mainly to clear START bit) */
		DMA_TCD_CSR(chn) = 0;
	}

	/* finally clear control register */
	DMA_CR = 0;
}

static __privileged inline void dma_init(void)
{
	/* enabled DMAMUX and DMA clock */
	BME_OR(SIM_SCGC6) = SIM_SCGC6_DMACHMUX;
	BME_OR(SIM_SCGC7) = SIM_SCGC7_DMA;

	dma_reset();
}

static __privileged inline void
dma_config(uint8_t chn, int8_t link_chn, bool irq, bool scatter_gather,
	   uint32_t saddr, uint8_t ssize, uint16_t soff, int32_t slast,
	   uint32_t daddr, uint8_t dsize, uint16_t doff, int32_t dlast,
	   uint16_t major_count, uint32_t minor_nbytes)
{
	uint16_t csr = 0, citer;

	compiletime_assert(ssize == 1 || ssize == 2 || ssize == 4,
			   "invalid source data transfer size");
	compiletime_assert(dsize == 1 || dsize == 2 || dsize == 4,
			   "invalid destination data transfer size");

	DMA_TCD_SADDR(chn) = saddr;
	DMA_TCD_SOFF(chn) = soff;
	DMA_TCD_ATTR(chn) = DMA_TCD_ATTR_DSIZE(__bf_shf(dsize)) |
			    DMA_TCD_ATTR_SSIZE(__bf_shf(ssize));
	DMA_TCD_NBYTES(chn) = minor_nbytes;
	DMA_TCD_SLAST(chn) = slast;
	DMA_TCD_DADDR(chn) = daddr;
	DMA_TCD_DOFF(chn) = doff;

	citer = DMA_TCD_CITER_ELINKNO_CITER(major_count);
	DMA_TCD_CITER(chn) = citer;

	DMA_TCD_DLASTSGA(chn) = dlast;

	if (irq)
		csr |= DMA_TCD_CSR_INTMAJOR;
	if (scatter_gather)
		csr |= DMA_TCD_CSR_ESG;
	if (link_chn >= 0)
		csr |= DMA_TCD_CSR_MAJORELINK |
		       DMA_TCD_CSR_MAJORLINKCH(link_chn);
	DMA_TCD_CSR(chn) = csr;

	DMA_TCD_BITER(chn) = citer;
}

static __privileged inline void
dma_set_minor_loop_size(uint8_t chn, uint32_t minor_nbytes)
{
	DMA_TCD_NBYTES(chn) = minor_nbytes;
}

static __privileged inline void
dma_set_major_count(uint8_t chn, uint16_t major_count)
{
	uint16_t citer = DMA_TCD_CITER_ELINKNO_CITER(major_count);

	DMA_TCD_CITER(chn) = citer;
	DMA_TCD_BITER(chn) = citer;
}

static __privileged inline void
dma_prepare_tcd(dma_tcd_t *tcd, int8_t link_chn, bool irq, bool scatter_gather,
		uint32_t saddr, uint8_t ssize, uint16_t soff, int32_t slast,
		uint32_t daddr, uint8_t dsize, uint16_t doff, int32_t dlast,
		uint16_t major_count, uint32_t minor_nbytes)
{
	compiletime_assert(ssize == 1 || ssize == 2 || ssize == 4,
			   "invalid source data transfer size");
	compiletime_assert(dsize == 1 || dsize == 2 || dsize == 4,
			   "invalid destination data transfer size");

	tcd->saddr = saddr;
	tcd->soff = soff;
	tcd->attr = DMA_TCD_ATTR_DSIZE(__bf_shf(dsize)) |
		    DMA_TCD_ATTR_SSIZE(__bf_shf(ssize));
	tcd->nbytes = minor_nbytes;
	tcd->slast = slast;
	tcd->daddr = daddr;
	tcd->doff = doff;
	tcd->citer = DMA_TCD_CITER_ELINKNO_CITER(major_count);
	tcd->dlastsga = dlast;

	tcd->csr = 0;
	if (irq)
		tcd->csr |= DMA_TCD_CSR_INTMAJOR;
	if (scatter_gather)
		tcd->csr |= DMA_TCD_CSR_ESG;
	if (link_chn >= 0)
		tcd->csr |= DMA_TCD_CSR_MAJORELINK |
			    DMA_TCD_CSR_MAJORLINKCH(link_chn);

	tcd->biter = tcd->citer;
}

static __privileged inline void dma_set_source(uint8_t chn, DMAMUX_Src_Type src)
{
	DMAMUX_CHCFG(chn) = DMAMUX_CHCFG_SOURCE(src) | DMAMUX_CHCFG_ENBL;
}

static __privileged inline void dma_enable_hwreq(uint8_t chn)
{
	DMA_SERQ = DMA_SERQ_SERQ(chn);
}

static __privileged inline void dma_disable_hwreq(uint8_t chn)
{
	DMA_CERQ = DMA_CERQ_CERQ(chn);
}

static __privileged inline void dma_disable_hwreq_sync(uint8_t chn)
{
	dma_disable_hwreq(chn);

	/* wait for channel active bit to clear */
	while (BME_BITFIELD(DMA_TCD_CSR(chn), DMA_TCD_CSR_ACTIVE))
		nop();
}

#endif /* DMA_H */

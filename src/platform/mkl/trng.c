#include "trng.h"
#include "debug.h"

#ifndef TRNG_TEST_HW_ERRORS
#error build system did not define TRNG_TEST_HW_ERRORS macro
#endif

__privileged void trng_init(void)
{
	/* enable TRNG clock */
	BME_OR(SIM_SCGC6) = SIM_SCGC6_TRNG;

	/* put TRNG into programming mode and reset registers */
	BME_OR(TRNG_MCTL) = TRNG_MCTL_PRGM;
	BME_OR(TRNG_MCTL) = TRNG_MCTL_RST_DEF;

	if (TRNG_TEST_HW_ERRORS) {
		/*
		 * set small monobit range to cause errors due to monobit
		 * statistical check failures
		 */
		uint16_t mono_range = 15;

		TRNG_SCML = TRNG_SCML_MONO_MAX(1250 + mono_range) |
			    TRNG_SCML_MONO_RNG(mono_range * 2);
	} else {
		/* set Von Neumann sampling mode */
		BME_BITFIELD(TRNG_MCTL, TRNG_MCTL_SAMP_MODE_MASK) =
			TRNG_MCTL_SAMP_MODE_VN;
	}

	/*
	 * If CPU is clocked too fast, then ring oscillator might run for too
	 * long and exceed the default FRQMAX limit.
	 * This will lead to FRQ_ERR and TRNG will block.
	 */
	BME_BITFIELD(TRNG_MCTL, TRNG_MCTL_OSC_DIV_MASK) = TRNG_MCTL_OSC_DIV_4;

	/* put trng into run mode */
	BME_AND(TRNG_MCTL) = ~TRNG_MCTL_PRGM;

	/* clear & enable TRNG error interrupts */
	BME_OR(TRNG_MCTL) = TRNG_MCTL_ERR;
	TRNG_INT_CTRL = 0;
	TRNG_INT_MASK = TRNG_INT_BIT_FRQ_CT_FAIL | TRNG_INT_BIT_HW_ERR;

	/* reset INTMUX channel 1 and mux TRNG IRQ */
	INTMUX_CHn_CSR(1) = INTMUX_CHn_CSR_RST;
	INTMUX_CHn_IER(1) = BIT(TRNG_IRQn);

	/*
	 * enable INTMUX channel 1 interrupt;
	 * TRNG irq has the same priority as SVC
	 */
	nvic_enable_irq_with_prio(INTMUX0_1_IRQn, 3);

	/* start entropy generation */
	BME_OR(TRNG_MCTL) = TRNG_MCTL_TRNG_ACC;
}

static __privileged void _trng_stop(void)
{
	/* disable and clear TRNG interrupts */
	nvic_disable_irq(INTMUX0_1_IRQn);
	TRNG_INT_MASK = 0;
	BME_OR(TRNG_MCTL) = TRNG_MCTL_ERR;

	/* stop entropy generation */
	BME_AND(TRNG_MCTL) = ~TRNG_MCTL_TRNG_ACC;
}

__privileged void trng_stop(void)
{
	/*
	 * don't touch TRNG registers if TRNG clock is not enabled;
	 * assume TRNG was not initialized
	 */
	if (!BME_BITFIELD(SIM_SCGC6, SIM_SCGC6_TRNG))
		return;

	/* stop the TRNG */
	_trng_stop();
}

/** Disable TRNG peripheral.
 * Request stop of the TRNG peripheral, waits for the ring oscillator to stop
 * and disables TRNG clock.
 */
static __privileged __maybe_unused void trng_disable(void)
{
	/*
	 * don't touch TRNG registers if TRNG clock is not enabled;
	 * assume TRNG was not initialized
	 */
	if (!BME_BITFIELD(SIM_SCGC6, SIM_SCGC6_TRNG))
		return;

	/* stop the TRNG */
	_trng_stop();

	/*
	 * wait for the TSTOP_OK bit before stopping clock so that the ring
	 * oscillator is not left running; this may take up to several seconds
	 */
	while (!BME_BITFIELD(TRNG_MCTL, TRNG_MCTL_TSTOP_OK))
		nop();

	/* disable TRNG clock */
	BME_AND(SIM_SCGC6) = ~SIM_SCGC6_TRNG;
}

/** TRNG IRQ handler.
 * Handles TRNG error interrupts by resetting the TRNG.
 */
void __irq trng_irq_handler(void)
{
	debug("TRNG error: MCTL=%08x, INT_STS=%08x\n",
	      TRNG_MCTL, TRNG_INT_STATUS);

	/* stop the TRNG */
	trng_stop();

	/* start TRNG again */
	trng_init();
}

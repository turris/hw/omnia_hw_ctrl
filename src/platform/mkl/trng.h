#ifndef TRNG_H
#define TRNG_H

#include "cpu.h"
#include "svc.h"

/** Initialize the TRNG peripheral.
 * Enables TRNG peripheral clock, configure registers for entropy generation,
 * enables interrupts and starts entropy generation.
 * Interrupt for TRNG is muxed at channel 1 of INTMUX.
 */
void trng_init(void);
SYSCALL(trng_init)

/** Stop TRNG peripheral.
 * Disables TRNG interrupts, clear erros and stops entropy generation.
 */
void trng_stop(void);

/** Get entropy availability status.
 * @returns true if 512 bits of entropy are avaiable, false otherwise
 */
static __privileged inline bool trng_ready(void)
{
	return BME_BITFIELD(TRNG_MCTL, TRNG_MCTL_ENT_VAL);
}

/** Read entropy from TRNG.
 * This will read 512 bits of entropy from TRNG after TRNG states that entropy
 * is valid. After entropy has been read, next entropy generation cycle is fired
 * automatically.
 * @param [out] entropy target to store entropy data
 * @returns true on success, false if entropy is not ready
 */
static __privileged inline bool
trng_collect_entropy(uint32_t entropy[static 16])
{
	if (!trng_ready())
		return false;

	for (unsigned int i = 0; i < 16; ++i)
		entropy[i] = TRNG_ENT(i);

	/* ENT(15) being read fires next entropy generation */

	return true;
}
SYSCALL(trng_collect_entropy, uint32_t *)

/** Synchronously wait for TRNG entropy and read it.
 * Wait until TRNG entropy is ready and then read it.
 * @param [out] entropy target to store entropy data
 */
static __privileged inline void
trng_collect_entropy_sync(uint32_t entropy[static 16])
{
	/* we assume at least one periodic interrupt is enabled */
	while (!trng_ready())
		wait_for_interrupt();

	trng_collect_entropy(entropy);
}

#endif /* TRNG_H */

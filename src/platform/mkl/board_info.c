#include "board_info.h"
#include "flash_plat.h"
#include "crc32.h"
#include "string.h"
#include "debug.h"

static board_info_t board_info __privileged_data;
static bool board_info_valid __privileged_data;

_Static_assert(sizeof(board_info_t) == 4 * sizeof(uint32_t),
	       "Board info code requires board_info_t to be 16 bytes long");

static __privileged uint8_t board_info_csum(const board_info_t *info)
{
	board_info_t copy = *info;
	uint32_t csum;

	/* set csum to zero so that we can compute csum for validation */
	copy.csum = 0;

	crc32(&csum, 0xffffffff, &copy, sizeof(copy));

	return csum & 0xff;
}

static __privileged bool read_slot(unsigned slot, board_info_t *info)
{
	uint32_t *ptr;

	if (slot >= 4) {
		debug("unsupported board info slot %u\n", slot);
		return false;
	}

	/* Board info is stored in Program Once Field with indexes
	 * 0 - 3 (slot 0), 4 - 7 (slot 1), 8 - 11 (slot 2) or 12 - 15 (slot 3).
	 */
	ptr = (uint32_t *)info;

	for (uint8_t idx = slot * 4; idx < slot * 4 + 4; ++idx)
		*ptr++ = flash_read_once_sync(idx);

	return board_info_csum(info) == info->csum;
}

__privileged void board_info_init(void)
{
	uint8_t *p;

	for (int slot = 3; slot >= 0; --slot) {
		if (read_slot(slot, &board_info)) {
			debug("Valid board info found at slot %d\n", slot);
			board_info_valid = true;
			break;
		}
	}

	if (!board_info_valid)
		return;

	debug("Board serial number: %08x%08x\n", board_info.serial_hi,
	      board_info.serial_lo);

	p = board_info.mac;
	debug("Board first MAC address: %02x:%02x:%02x:%02x:%02x:%02x\n",
	      p[0], p[1], p[2], p[3], p[4], p[5]);

	debug("Board revision: %u\n", board_info.revision);
}

__privileged bool board_info_get(board_info_t *info)
{
	if (board_info_valid)
		memcpy(info, &board_info, sizeof(board_info));

	return board_info_valid;
}

static __privileged bool can_burn_into_slot(unsigned slot)
{
	if (slot >= 4) {
		debug("unsupported board info slot %u\n", slot);
		return false;
	}

	for (uint8_t idx = slot * 4; idx < slot * 4 + 4; ++idx)
		if (flash_read_once_sync(idx) != 0xffffffff)
			return false;

	return true;
}

static __privileged bool
burn_into_slot(unsigned slot, const board_info_t *info)
{
	const uint32_t *ptr = (const uint32_t *)info;

	if (slot >= 4) {
		debug("unsupported board info slot %u\n", slot);
		return false;
	}

	for (uint8_t idx = slot * 4; idx < slot * 4 + 4; ++idx)
		if (!flash_program_once_sync(idx, *ptr++))
			return false;

	return true;
}

__privileged bool board_info_burn(const board_info_t *info)
{
	if (board_info_valid)
		return false;

	if (board_info_csum(info) != info->csum)
		return false;

	for (unsigned slot = 0; slot < 4; ++slot) {
		board_info_t validation;

		if (!can_burn_into_slot(slot))
			continue;

		if (!burn_into_slot(slot, info))
			return false;

		if (!read_slot(slot, &validation))
			return false;

		if (memcmp(info, &validation, sizeof(*info)))
			return false;

		memcpy(&board_info, info, sizeof(*info));
		board_info_valid = true;

		break;
	}

	return true;
}

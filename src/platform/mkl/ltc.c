#include "ltc.h"
#include "dma.h"
#include "string.h"
#include "debug.h"

#define LTC_DS_MAX		(LTC_DS_DS_MASK >> __bf_shf(LTC_DS_DS_MASK))
#define LTC_MD_BLOCK_SIZE	64
#define LTC_MD_BLOCK_WORDS	(LTC_MD_BLOCK_SIZE / 4)
#define SHA256_CTX_WORDS	(SHA256_CTX_SIZE / 4)

#define ECDSA_BUFFER_WORDS	(SHA256_CTX_WORDS + 1 + 2 * PKHA_NUMBER_WORDS)
#define HMAC_BUFFER_WORDS	(LTC_MD_BLOCK_WORDS + ECDSA_BUFFER_WORDS)

__privileged void ltc_init(void)
{
	/* enable LTC clock */
	BME_OR(SIM_SCGC5) = SIM_SCGC5_LTC;

	/* Reset all internal logic */
	LTC_COM = LTC_COM_ALL;

	/* Enable interrupts */
	nvic_enable_irq_with_prio(LTC0_IRQn, 2);

	/* source LTC IFIFO on DMA channel 0 */
	dma_set_source(DMACHAN_LTC_IFIFO, DMAMUX_Src_LTC_IFIFO);
	dma_enable_hwreq(DMACHAN_LTC_IFIFO);
}

/** Get and clear status of LTC being done with computation.
 * @returns true if LTC is done with computation, false otherwise.
 */
static __privileged inline bool ltc_done_clear()
{
	return BME_LOAD_SET(LTC_STA, LTC_STA_DI);
}

static struct {
	bool busy; /* ltc_priv busy indicator */
	uint32_t status; /* cached LTC_STA (clearing DI flag clears status) */

	/* next state in LTC IRQ handler */
	enum ltc_state_e {
		LTC_STATE_INVALID = 0,

		SHA256_CONTINUE,
		SHA256_CALLBACK,

		HMAC_SHA256_SECOND_ROUND,

		SIGNING_BEGIN,
		SIGNING_K1,
		SIGNING_V1,
		SIGNING_K2,
		SIGNING_V2,
		SIGNING_RAND_MUL_G,
		SIGNING_R_MOD,
		SIGNING_MUL_PRIV,
		SIGNING_Z_MOD,
		SIGNING_PLUS_Z,
		SIGNING_TMP_SAVE,
		SIGNING_RAND_INV,
		SIGNING_TMP_LOAD,
		SIGNING_S,
		SIGNING_CLEAR,
		SIGNING_DONE,

		VERIFYING_BEGIN,
		VERIFYING_Z_MOD,
		VERIFYING_U1,
		VERIFYING_U1_MOV,
		VERIFYING_U1_MUL_G,
		VERIFYING_U2,
		VERIFYING_U2_MOV,
		VERIFYING_U2_MUL_PUB_KEY,
		VERIFYING_ADD,
		VERIFYING_CLEAR,
		VERIFYING_COMPARE,

		PUBLIC_KEY_CLEAR,
		PUBLIC_KEY_DONE,
	} next_state; /* next state in LTC IRQ handler */

	/* SHA256 message digest internal structure */
	struct {
		/* the state to which the LTC IRQ handler shall go when message
		 * is digested
		 */
		enum ltc_state_e finish_state;
		uint16_t remaining; /* how many bytes are remaining to digest */
	} md;

	/* HMAC SHA256 internal structure */
	struct {
		/* the state to which the LTC IRQ handler shall go when HMAC
		 * digest is done
		 */
		enum ltc_state_e finish_state;

		/* internal buffer to pass to SHA256 */
		union {
			uint8_t buffer[HMAC_BUFFER_WORDS * 4];
			uint32_t buffer_words[HMAC_BUFFER_WORDS];
		};
	} hmac;

	union {
		struct {
			const pkha_curve_t *curve;
			const pkha_number_t *priv_key;
			const void *msg;
			pkha_number_t z;
			uint32_t K[SHA256_CTX_WORDS];
			union {
				uint8_t buffer[ECDSA_BUFFER_WORDS * 4];
				uint32_t buffer_words[ECDSA_BUFFER_WORDS];
			};
			pkha_number_t k;
			pkha_signature_t signature;
		} sign;
		struct {
			const pkha_curve_t *curve;
			const pkha_point_t *pub_key;
			pkha_number_t z;
			pkha_signature_t signature;
			pkha_number_t tmp;
			pkha_point_t u1g;
		} verify;
	};

	ltc_callback_t callback; /* callback for LTC operation */
	void *result; /* pointer to LTC operation result */
} ltc_priv __privileged_data;

/** Try to acquire hold over ltc_priv, storing callback and result pointers
 * @param callback the pointer to the function which will be called on release;
 *        if NULL, the callback won't be called
 * @param result the parameter for the callback function
 * @returns true if successfuly acquired, false if busy
 */
static __privileged bool ltc_try_acquire(ltc_callback_t callback, void *result)
{
	bool res;

	disable_irq();

	res = !ltc_priv.busy;
	if (res) {
		ltc_priv.busy = true;
		ltc_priv.callback = callback;
		ltc_priv.result = result;
	}

	enable_irq();

	return res;
}

/** Release ltc_priv and call the callback function stored in acquire */
static __privileged void ltc_release_and_callback(void)
{
	ltc_callback_t callback = ltc_priv.callback;
	void *result = ltc_priv.result;

	/* forget callback and result pointer, for security */
	ltc_priv.callback = NULL;
	ltc_priv.result = NULL;

	/* release LTC */
	ltc_priv.busy = false;

	/* call back */
	if (callback)
		callback(result);
}

__privileged void ltc_reset(void)
{
	ltc_callback_t callback = NULL;

	disable_irq();

	/* Reset all internal logic */
	LTC_COM = LTC_COM_ALL;

	/* if busy, there may be a callback to be informed about the reset */
	if (ltc_priv.busy)
		callback = ltc_priv.callback;

	/* zero-out the state (also clears busy flag) */
	bzero(&ltc_priv, sizeof(ltc_priv));

	enable_irq();

	/* if there is a callback, call with NULL to indicate failure */
	if (callback)
		callback(NULL);
}

/** Determine next chunk size for message digest; decrease ltc_state.remaining.
 * The LTC_DS register is 12 bit wide, the maximum value to be written is 4095.
 * But non-final MD methods must pass multiples of 64, so the maximum value for
 * LTC_MD_AS_INIT / LTC_MD_AS_UPDATE is 4032.
 * For LTC_MD_AS_INIT_FIN / LTC_MD_AS_FINALIZE we can pass any length up to
 * 4095, it does not have to be a multiple of 64.
 * @returns next chunk size
 */
static __privileged uint16_t ltc_md_next_chunk_size(void)
{
	uint16_t chunk_size;

	if (ltc_priv.md.remaining <= LTC_DS_MAX)
		chunk_size = ltc_priv.md.remaining;
	else
		chunk_size = (LTC_DS_MAX / LTC_MD_BLOCK_SIZE) *
			     LTC_MD_BLOCK_SIZE;

	ltc_priv.md.remaining -= chunk_size;

	return chunk_size;
}

/** Determine DMA minor loop size.
 * Try to get larger value (up to 16, the size of LTC input FIFO) if possible.
 * @returns DMA minor loop size in bytes
 */
static __privileged uint8_t ltc_md_dma_minor_size(uint16_t chunk_size)
{
	if (chunk_size & 7)
		return 4;
	else if (chunk_size & 8)
		return 8;
	else
		return 16; /* LTC input FIFO size */
}

/** Start LTC SHA256 message digest.
 * @returns true if started succesfully, false if LTC is busy
 */
static __privileged void
ltc_sha256_start(const void *data, uint16_t length, enum ltc_state_e finish_state)
{
	uint16_t chunk_size, major_count, minor_size;
	uint32_t addr = (uint32_t)data;
	uint8_t ssize, op;

	/* set remaining length */
	ltc_priv.md.remaining = length;
	ltc_priv.md.finish_state = finish_state;

	/* get first chunk size */
	chunk_size = ltc_md_next_chunk_size();

	/* prepare minor loop size and major loop count */
	minor_size = ltc_md_dma_minor_size(chunk_size);
	major_count = DIV_ROUND_UP(chunk_size, minor_size);

	/*
	 * Determine source data transfer size, based on data alignment:
	 * If data is 4-byte aligned, transfer words, if it is 2-byte aligned,
	 * transfer half-words, otherwise transfer bytes.
	 */
	if (addr & 1)
		ssize = 1;
	else if (addr & 2)
		ssize = 2;
	else
		ssize = 4;

	/* configure DMA */
	dma_config(DMACHAN_LTC_IFIFO, -1, true, false, addr, ssize, ssize, 0,
		   (uint32_t)&LTC_IFIFO, 4, 0, 0, major_count, minor_size);

	/* nuke everything LTC */
	LTC_COM = LTC_COM_ALL;

	/*
	 * Byte swap inputs and outputs. LTC native endianess is different than
	 * that of the CPU. Enable input FIFO DMA requesting.
	 */
	BME_OR(LTC_CTL) = LTC_CTL_COS | LTC_CTL_CIS | LTC_CTL_KOS |
			  LTC_CTL_KIS | LTC_CTL_OFS | LTC_CTL_IFS |
			  LTC_CTL_IFE | LTC_CTL_IFR;

	if (ltc_priv.md.remaining) {
		op = LTC_MD_AS_INIT;
		ltc_priv.next_state = SHA256_CONTINUE;
	} else {
		op = LTC_MD_AS_INIT_FIN;
		ltc_priv.next_state = ltc_priv.md.finish_state;
	}

	/* start digesting */
	LTC_MD = LTC_MD_ALG_MDHA_SHA_256 | op;
	LTC_DS = chunk_size;
}

/** Continue LTC SHA256 message digest. */
static __privileged void ltc_sha256_continue(void)
{
	uint16_t chunk_size;
	uint8_t op;

	/* get next chunk size */
	chunk_size = ltc_md_next_chunk_size();

	if (ltc_priv.md.remaining) {
		op = LTC_MD_AS_UPDATE;
	} else {
		uint16_t major_count, minor_size;

		op = LTC_MD_AS_FINALIZE;
		ltc_priv.next_state = ltc_priv.md.finish_state;

		/*
		 * If this is last transfer, the chunk size may be different
		 * from the last one. Recompute minor loop size and major loop
		 * count.
		 */

		minor_size = ltc_md_dma_minor_size(chunk_size);
		major_count = DIV_ROUND_UP(chunk_size, minor_size);

		dma_set_minor_loop_size(DMACHAN_LTC_IFIFO, minor_size);
		dma_set_major_count(DMACHAN_LTC_IFIFO, major_count);
	}

	/*
	 * Clear mode and data size. Data size is not automatically cleared when
	 * mode is changed, so the peripheral expects more data to be processed
	 * than reality.
	 */
	LTC_CW = LTC_CW_CM | LTC_CW_CDS;

	/* continue digesting */
	LTC_MD = LTC_MD_ALG_MDHA_SHA_256 | op;
	LTC_DS = chunk_size;
}

/** Collect SHA256 message digest, clear context and call back */
static __privileged void ltc_sha256_done(void)
{
	uint32_t *result = ltc_priv.result;

	/* store the context into the result */
	for (unsigned i = 0; i < SHA256_CTX_WORDS; ++i)
		put_unaligned32(LTC_CTX(i), &result[i]);

	/* clear context */
	BME_OR(LTC_CW) = LTC_CW_CCR;

	ltc_release_and_callback();
}

static __privileged void
_ltc_hmac_sha256_start(const void *key, uint8_t klen,
		       const void *data, uint8_t dlen,
		       enum ltc_state_e finish_state)
{
	typeof(ltc_priv.hmac) *hmac = &ltc_priv.hmac;

	hmac->finish_state = finish_state;

	/* save key into the buffer, padded with zeros */
	memcpy(hmac->buffer, key, klen);
	bzero(&hmac->buffer[klen], LTC_MD_BLOCK_SIZE - klen);

	/* xor key with ipad */
	for (unsigned i = 0; i < LTC_MD_BLOCK_WORDS; ++i)
		hmac->buffer_words[i] ^= 0x36363636;

	/* append data */
	memcpy(&hmac->buffer[LTC_MD_BLOCK_SIZE], data, dlen);

	/* start first round */
	ltc_sha256_start(hmac->buffer, LTC_MD_BLOCK_SIZE + dlen,
			 HMAC_SHA256_SECOND_ROUND);
}

/** Start SHA256 HMAC computation.
 * Starts computation of SHA256 HMAC of @ref data of length @ref dlen (at most
 * 100), with key @reg key of length @ref klen (at most 64).
 * When the computation is done, the state machine will go to @ref finish_state.
 */
static __privileged __force_inline void
ltc_hmac_sha256_start(const void *key, uint8_t klen,
		      const void *data, uint8_t dlen,
		      enum ltc_state_e finish_state)
{
	compiletime_assert(klen <= LTC_MD_BLOCK_SIZE,
			   "key too long");
	compiletime_assert(dlen <= HMAC_BUFFER_WORDS * 4 - LTC_MD_BLOCK_SIZE,
			   "data too long");

	_ltc_hmac_sha256_start(key, klen, data, dlen, finish_state);
}

/** Store LTC message digest context into 4-byte aligned buffer.
 * @param dst pointer to a 4-byte aligned buffer.
 * @returns pointer to the end of stored context
 */
static __privileged void *ltc_store_ctx_aligned(void *dst)
{
	uint32_t *ptr = dst;

	for (unsigned i = 0; i < SHA256_CTX_WORDS; ++i)
		*ptr++ = LTC_CTX(i);

	return ptr;
}

/** Run second round of HMAC */
static __privileged void ltc_hmac_sha256_second_round(void)
{
	typeof(ltc_priv.hmac) *hmac = &ltc_priv.hmac;
	uint32_t *ptr = hmac->buffer_words;

	/* revert xoring key with ipad and xor with opad */
	for (unsigned i = 0; i < LTC_MD_BLOCK_WORDS; ++i)
		*ptr++ ^= 0x36363636 ^ 0x5c5c5c5c;

	/* append first round result */
	ptr = ltc_store_ctx_aligned(ptr);

	/* forget the rest of data in the buffer, for security reasons */
	bzero(ptr, sizeof(hmac->buffer) - (LTC_MD_BLOCK_SIZE +
					   SHA256_CTX_SIZE));

	/* start second round */
	ltc_sha256_start(hmac->buffer, LTC_MD_BLOCK_SIZE + SHA256_CTX_SIZE,
			 hmac->finish_state);
}

static __privileged void ltc_ecdsa_sign_handler(enum ltc_state_e state);
static __privileged void ltc_ecdsa_verify_handler(enum ltc_state_e state);
static __privileged void ltc_ecdsa_public_key_handler(enum ltc_state_e state);

/** LTC IRQ handler.
 * Handles LTC done interrupt.
 */
void __irq ltc_irq_handler(void)
{
	enum ltc_state_e state;

	/* error is unlikely */
	if (unlikely(BME_BITFIELD(LTC_STA, LTC_STA_EI))) {
		debug("LTC error: STA=%08x ESTA=%08x\n", LTC_STA, LTC_ESTA);
		return ltc_reset();
	}

	/* save status from LTC_STA, since clearing done flag clears status */
	ltc_priv.status = LTC_STA;

	if (!ltc_done_clear()) {
		debug("unhandled\n");
		return;
	}

	state = ltc_priv.next_state;

	switch (state) {
	case SHA256_CONTINUE:
		ltc_sha256_continue();
		break;

	case SHA256_CALLBACK:
		ltc_sha256_done();
		break;

	case HMAC_SHA256_SECOND_ROUND:
		ltc_hmac_sha256_second_round();
		break;

	default:
		if (state >= SIGNING_BEGIN && state <= SIGNING_DONE)
			ltc_ecdsa_sign_handler(state);
		else if (state >= VERIFYING_BEGIN && state <= VERIFYING_COMPARE)
			ltc_ecdsa_verify_handler(state);
		else if (state >= PUBLIC_KEY_CLEAR && state <= PUBLIC_KEY_DONE)
			ltc_ecdsa_public_key_handler(state);
		else
			debug("invalid LTC state %u\n", state);
		break;
	}
}

__privileged bool ltc_sha256_async(void *hash, const void *data,
				   uint16_t length, ltc_callback_t callback)
{
	if (!ltc_try_acquire(callback, hash))
		return false;

	ltc_sha256_start(data, length, SHA256_CALLBACK);

	return true;
}

typedef struct {
	uint8_t hash[SHA256_CTX_SIZE];
	volatile bool done;
} ltc_sha256_sync_t;

static __privileged void ltc_sha256_cb(void *ptr)
{
	ltc_sha256_sync_t *result = ptr;

	result->done = true;
}

__privileged bool ltc_sha256(void *hash, const void *data, uint16_t length)
{
	ltc_sha256_sync_t result;

	result.done = false;

	if (!ltc_sha256_async(&result, data, length, ltc_sha256_cb))
		return false;

	while (!result.done)
		wait_for_interrupt();

	memcpy(hash, result.hash, sizeof(result.hash));

	return true;
}

#define LTC_PKHA_ENGINE_PKHA		0x800000

/* Function codes for PKHA functions. Stated here as they
 * span multiple LTC_PKMD fields.
 */
#define LTC_PKHA_FUNC_CLEAR_ALL		(LTC_PKHA_ENGINE_PKHA | 0xF0001)
#define LTC_PKHA_FUNC_COPY_SSZ		(LTC_PKHA_ENGINE_PKHA | 0x00011)

#define LTC_PKHA_FUNC_ADD		(LTC_PKHA_ENGINE_PKHA | 0x00002)
#define LTC_PKHA_FUNC_SUB_1		(LTC_PKHA_ENGINE_PKHA | 0x00003)
#define LTC_PKHA_FUNC_SUB_2		(LTC_PKHA_ENGINE_PKHA | 0x00004)
#define LTC_PKHA_FUNC_MUL		(LTC_PKHA_ENGINE_PKHA | 0x00405)
#define LTC_PKHA_FUNC_EXP		(LTC_PKHA_ENGINE_PKHA | 0x00406)
#define LTC_PKHA_FUNC_AMODN		(LTC_PKHA_ENGINE_PKHA | 0x00007)
#define LTC_PKHA_FUNC_INV		(LTC_PKHA_ENGINE_PKHA | 0x00008)
#define LTC_PKHA_FUNC_R2		(LTC_PKHA_ENGINE_PKHA | 0x0000C)
#define LTC_PKHA_FUNC_GCD		(LTC_PKHA_ENGINE_PKHA | 0x0000E)
#define LTC_PKHA_FUNC_PRIME		(LTC_PKHA_ENGINE_PKHA | 0x0000F)

#define LTC_PKHA_FUNC_ECC_MOD_ADD	(LTC_PKHA_ENGINE_PKHA | 0x00009)
#define LTC_PKHA_FUNC_ECC_MOD_DBL	(LTC_PKHA_ENGINE_PKHA | 0x0000A)
#define LTC_PKHA_FUNC_ECC_MOD_MUL	(LTC_PKHA_ENGINE_PKHA | 0x0040B)

#define LTC_PKHA_DEST_A			0x00100

typedef enum {
	REG_A = 0,
	REG_B = 1,
	REG_E = 2,
	REG_N = 3,
} pkha_reg_num_t;

#define _REG_QUADRANT(_r, _q) (((REG_ ## _r) << 2) | ((_q) & 3))

typedef enum {
	A0 = _REG_QUADRANT(A, 0),
	A1 = _REG_QUADRANT(A, 1),
	A2 = _REG_QUADRANT(A, 2),
	A3 = _REG_QUADRANT(A, 3),

	B0 = _REG_QUADRANT(B, 0),
	B1 = _REG_QUADRANT(B, 1),
	B2 = _REG_QUADRANT(B, 2),
	B3 = _REG_QUADRANT(B, 3),

	E = _REG_QUADRANT(E, 0),

	N0 = _REG_QUADRANT(N, 0),
	N1 = _REG_QUADRANT(N, 1),
	N2 = _REG_QUADRANT(N, 2),
	N3 = _REG_QUADRANT(N, 3),
} pkha_reg_t;

static __force_inline pkha_reg_num_t
pkha_reg_num(pkha_reg_t reg)
{
	return reg >> 2;
}

static __force_inline uint8_t
pkha_reg_quadrant(pkha_reg_t reg)
{
	return reg & 3;
}

static __force_inline volatile uint32_t *
pkha_reg_size(pkha_reg_t reg)
{
	switch (pkha_reg_num(reg)) {
	case REG_A: return &LTC_PKASZ;
	case REG_B: return &LTC_PKBSZ;
	case REG_E: return &LTC_PKESZ;
	case REG_N: return &LTC_PKNSZ;
	default: unreachable();
	}
}

static __force_inline volatile uint32_t *
pkha_reg_ptr(pkha_reg_t reg)
{
	uint8_t quad = pkha_reg_quadrant(reg);

	switch (pkha_reg_num(reg)) {
	case REG_A: return &LTC_PKA(quad, 0);
	case REG_B: return &LTC_PKB(quad, 0);
	case REG_E: return &LTC_PKE(0);
	case REG_N: return &LTC_PKN(quad, 0);
	default: unreachable();
	}
}

/* Dumps LTC register.
 * @param what string description of register content (e.g. key, curve a, etc.)
 * @param reg register to be dumped
 */
static __privileged __maybe_unused __force_inline void
ltc_dump_register(pkha_reg_t reg, const char *what)
{
	static const char reg_name[4] = { 'A', 'B', 'E', 'N' };
	volatile const uint32_t *ptr = pkha_reg_ptr(reg);
	pkha_reg_num_t num = pkha_reg_num(reg);
	uint8_t quad = pkha_reg_quadrant(reg);
	uint8_t size;

	compiletime_assert(reg != E, "register E is write-only");

	size = *pkha_reg_size(reg);
	debug("%s (%c%u):", what, reg_name[num], quad);

	for (int i = DIV_ROUND_UP(size, 4) - 1; i >= 0; --i)
		debug(" %08x", ptr[i]);
	debug("\n");
}

/** Execute LTC command and wait for completion.
 * Will execute LTC command. For commands which allow choosing the destination
 * register it allows to choose either A or B as the command output.
 * @param dst if command supports it, allows choosing between A or B as
 *        command output register. Use values of A0 or B0.
 * @param cmd value to be written into LTC_MDPK register.
 */
static __privileged void _ltc_pkha_exec(pkha_reg_t dst, uint32_t cmd)
{
	compiletime_assert(pkha_reg_num(dst) == REG_A ||
			   pkha_reg_num(dst) == REG_B,
			   "unsupported destination register");

	if (pkha_reg_num(dst) == REG_A)
		cmd |= LTC_PKHA_DEST_A;

	LTC_MDPK = cmd;
}

/** Loads LTC PKHA register quadrant from value stored in RAM.
 * @note If source address passed is NULL, then the load is skipped.
 * @param dst LTC register to store data into. If NULL, the load is skipped.
 * @param src address of RAM location containing value to be loaded into
 *        register
 */
static __privileged void ltc_load(pkha_reg_t dst, const pkha_number_t *src)
{
	volatile uint32_t *ptr = pkha_reg_ptr(dst);

	if (!src)
		return;

	*pkha_reg_size(dst) = PKHA_NUMBER_SIZE;
	for_each(word, src->words)
		*ptr++ = *word;
}

/** Stores value from LTC PKHA register quadrant into RAM.
 * Will store content of register into RAM. Register E is write only, so any
 * attempt to read from it will fail.
 * @param dst address of RAM location where the value has to be stored
 * @param src LTC register to read data from
 * @note Register E is read protected, therefore we prohibit reading it.
 */
static __privileged void ltc_store(pkha_number_t *dst, pkha_reg_t src)
{
	volatile const uint32_t *ptr = pkha_reg_ptr(src);

	compiletime_assert(src != E, "register E is write-only");

	for_each(word, dst->words)
		*word = *ptr++;
}

/** Copy data from one PKHA register into another.
 * Will copy data from one register or its quadrant into another register or
 * quadrant. Size of the operation is determined either by the size of source.
 * If size of the value copied is larger than destination quadrant, it will
 * spill into next quadrant.
 * Register E is write-only and not divided into quadrants so LTC does not
 * provide command for copying data from non-zero quadrant of any register into
 * register E, thus any operation targeting E must copy from quadrant 0.
 * Reads from register E will deliberately fail.
 * @param dst register where the data has to be copied
 * @param src register which contains data to be copied
 */
static __privileged void ltc_mov(pkha_reg_t dst, pkha_reg_t src)
{
	uint32_t cmd;

	compiletime_assert(src != dst,
			   "source and destination must not be same");
	compiletime_assert(src != E, "register E is write-only");
	compiletime_assert(dst != E || pkha_reg_quadrant(src) == 0,
			   "register E must be loaded from quadrant 0");

	cmd = (pkha_reg_num(src) << 17) | (pkha_reg_num(dst) << 10) |
	      (pkha_reg_quadrant(src) << 8) | (pkha_reg_quadrant(dst) << 6) |
	      LTC_PKHA_FUNC_COPY_SSZ;

	LTC_MDPK = cmd;
}

static __privileged inline void ltc_pkha_clear_all(void)
{
	LTC_MDPK = LTC_PKHA_FUNC_CLEAR_ALL;
}

/*
 * Following are functions that provide access to LTC PKHA operations in
 * the form of pseudo-assembly. Almost all of LTC operations have implicit
 * arguments stored in pre-determined and fixed LTC registers (buffers).
 * Arithmetic operations allow to select the output register to be either
 * A (or a pair of its quadrants) or B (similarly).
 *
 * These provide low level interface to the PKHA functionality implementing
 * elliptic curve cryptography.
 *
 * As they are pseudo-assembly instructions, the code using them looks like
 * a piece of assembly code. It is strongly advised against using them directly
 * to implement more complex pieces of code. Use wrapper functions instead.
 */

/** Modular addition.
 * Will calculate (A + B) mod N. Values are taken from A, B and N registers.
 * Result may be stored either into A or B register. Size or operation depends
 * on the size of value stored in the N register. If operands are larger than
 * N, then the operation will fail.
 * @param dst output register; may only be A0 or B0 register
 */
static __privileged inline void _ltc_mod_add(pkha_reg_t dst)
{
	_ltc_pkha_exec(dst, LTC_PKHA_FUNC_ADD);
}

/** Modular subtraction.
 * Will calculate either (A - B) mod N or (B - A) mod N. Values are taken from
 * A, B and N registers. Result may be stored either into A or B register. Size
 * of operation depends on the size of value stored in the N register.
 * @param dst output register; may only be A0 or B0 register
 * @param min minuend; may only be A0 or B0 register
 * @param sub subtrahend; may only be A0 or B0 register
 */
static __privileged inline void
_ltc_mod_sub(pkha_reg_t dst, pkha_reg_t min, pkha_reg_t sub)
{
	uint32_t cmd;

	compiletime_assert((min == A0 && sub == B0) ||
			   (min == B0 && sub == A0),
			   "invalid arguments");

	if (min == A0 && sub == B0)
		cmd = LTC_PKHA_FUNC_SUB_1;
	else
		cmd = LTC_PKHA_FUNC_SUB_2;

	_ltc_pkha_exec(dst, cmd);
}

/** Modular multiplication.
 * Will calculate (A * B) mod N. Values are taken from A, B and N registers.
 * Result may be stored either into A or B register. Size or operation depends
 * on the size of value stored in the N register. If operands are larger than
 * N, then the operation will fail.
 * @note This call performs timing equalized variant of modular multiplication.
 * @param dst output register; may only be A0 or B0 register
 */
static __privileged inline void _ltc_mod_mul(pkha_reg_t dst)
{
	_ltc_pkha_exec(dst, LTC_PKHA_FUNC_MUL);
}

/** Modular exponentiation.
 * Will calculate (A ^ E) mod N. Values are taken from A, E and N registers.
 * Result may be stored either into A or B register. Size or operation depends
 * on the size of value stored in the N register. If operands are larger than
 * N, then the operation will fail.
 * @param dst output register; may only be A0 or B0 register
 */
static __privileged inline void _ltc_mod_exp(pkha_reg_t dst)
{
	_ltc_pkha_exec(dst, LTC_PKHA_FUNC_EXP);
}

/** Calculate modulo of the value.
 * Will calculate A mod N. Values are taken from A and N registers.
 * Result may be stored either into A or B register. Size of value in A register
 * may be larger than the size of the value in the N register.
 * @param dst output register; may only be A0 or B0 register
 */
static __privileged inline void _ltc_mod_amodn(pkha_reg_t dst)
{
	_ltc_pkha_exec(dst, LTC_PKHA_FUNC_AMODN);
}

/* B | A <- A^-1 mod N */
/** Modular inversion.
 * Will calculate (A^-1) mod N. Values are taken from A and N registers.
 * Result may be stored either into A or B register. Size or operation depends
 * on the size of value stored in the N register. If operands are larger than
 * N, then the operation will fail.
 * @param dst output register; may only be A0 or B0 register
 */
static __privileged inline void _ltc_mod_inv(pkha_reg_t dst)
{
	_ltc_pkha_exec(dst, LTC_PKHA_FUNC_INV);
}

/* B | A <- R2(N) */
/** Calculate R2 factor.
 * Will calculate R2 factor for value N. Value is taken register N.
 * Result may be stored either into A or B register. Size or operation depends
 * on the size of value stored in the N register.
 * @param dst output register; may only be A0 or B0 register
 */
static __privileged inline void _ltc_mod_r2(pkha_reg_t dst)
{
	_ltc_pkha_exec(dst, LTC_PKHA_FUNC_R2);
}

/* B | A <- GCD(A, N) */
/** Calculate greatest common divisor of two values.
 * Will calculate greatest common divisor of A and N. Values are taken from
 * A and N register. Size of the operation depends on the size of value
 * stored in the N register.
 * @param dst output register; may only be A0 or B0 register
 */
static __privileged inline void _ltc_mod_gcd(pkha_reg_t dst)
{
	_ltc_pkha_exec(dst, LTC_PKHA_FUNC_GCD);
}

/* B | A <- is_prime(N, A, B) */
/** Test if value is a prime.
 * Will perform Miller-Rabin primarility test of value stored in register N.
 * Uses random seed value stored in register A to run amount of trials stored
 * in the lowest byte of register B. If register B contains value of 0, one
 * trial will still be ran. Value in A must be less than the value of N-2 and
 * might not be zero.
 * @param dst output register; may only be A0 or B0 register
 */
static __privileged inline void _ltc_prime_test(pkha_reg_t dst)
{
	_ltc_pkha_exec(dst, LTC_PKHA_FUNC_PRIME);
}

/** Elliptic curve point addition.
 * Will perform sum of two points on an elliptic curve. Points added have their
 * coordinates stored in [A0, A1] and [B1, B2] registers. Curve parameters are
 * stored in A3 (a parameter) and B0 (b parameter) register. Register N contains
 * field modulus and determines the size of the operation. If any operand is
 * larger than N operation will fail. It will fail for any value of N which is
 * longer than 64 bytes or even.
 * If A0 is chosen as result output, then resulting point is stored in [A0, A1]
 * register pair, while pair of [B1, B2] is used for B0 chosen as the output.
 * @param dst output register; may only be A0 or B0 register
 */
static __privileged inline void _ltc_ecc_mod_add(pkha_reg_t dst)
{
	_ltc_pkha_exec(dst, LTC_PKHA_FUNC_ECC_MOD_ADD);
}

/** Elliptic curve point duplication.
 * Will perform sum of point with itself on an elliptic curve. Point added has
 * its coordinates stored in [B1, B2] registers. Curve parameters are stored in
 * A3 (a parameter) and B0 (b parameter) register. Register N contains field
 * modulus and determines the size of the operation. If any operand is larger
 * than N operation will fail. It will fail for any value of N which is longer
 * than 64 bytes or even.
 * If A0 is chosen as result output, then resulting point is stored in [A0, A1]
 * register pair, while pair of [B1, B2] is used for B0 chosen as the output.
 * @param dst output register; may only be A0 or B0 register
 */
static __privileged inline void _ltc_ecc_mod_dbl(pkha_reg_t dst)
{
	_ltc_pkha_exec(dst, LTC_PKHA_FUNC_ECC_MOD_DBL);
}

/** Elliptic curve scalar point multiplication.
 * Will perform multiplication of scalar and point on an elliptic curve. Scalar
 * by which the point is multiplied is stored in the E register, the point has
 * its coordinates stored in [B1, B2] registers. Curve parameters are stored in
 * A3 (a parameter) and B0 (b parameter) register. Register N contains field
 * modulus and determines the size of the operation. If any operand is larger
 * than N operation will fail. It will fail for any value of N which is longer
 * than 64 bytes or even.
 * If A0 is chosen as result output, then resulting point is stored in [A0, A1]
 * register pair, while pair of [B1, B2] is used for B0 chosen as the output.
 * @note This call performs timing equalized variant of modular multiplication.
 * @param dst output register; may only be A0 or B0 register
 */
static __privileged inline void _ltc_ecc_mod_mul(pkha_reg_t dst)
{
	_ltc_pkha_exec(dst, LTC_PKHA_FUNC_ECC_MOD_MUL);
}

/* Following functions form higher level API to access LTC functions.
 * They combine LTC operation with code to load the function operands.
 * This makes the code using these functions a lot less spaghetti-like
 * and better understandable.
 */

/* Load arguments and perform A + B mod N
 * @param dst output register, A0 or B0 are valid here
 * @param a address of value for the first addend
 * @param b address of value for the second addend
 * @param modulo address od value of modulo
 */
static __privileged __maybe_unused void
ltc_mod_add(pkha_reg_t dst, const pkha_number_t *a, const pkha_number_t *b,
	    const pkha_number_t *modulo)
{
	ltc_load(A0, a);
	ltc_load(B0, b);
	ltc_load(N0, modulo);

	_ltc_mod_add(dst);
}

/* Load arguments and perform A - B mod N
 * @param dst output register, A0 or B0 are valid here
 * @param a address of value for the minuend
 * @param b address of value for the subtrahend
 * @param modulo address od value of modulo
 */
static __privileged __maybe_unused void
ltc_mod_sub(pkha_reg_t dst, const pkha_number_t *a, const pkha_number_t *b,
	    const pkha_number_t *modulo)
{
	ltc_load(A0, a);
	ltc_load(B0, b);
	ltc_load(N0, modulo);

	_ltc_mod_sub(dst, A0, B0);
}

/* Load arguments and perform A * B mod N
 * @param dst output register, A0 or B0 are valid here
 * @param a address of value for the first multiplicant
 * @param b address of value for the second multiplicant
 * @param modulo address od value of modulo
 */
static __privileged void
ltc_mod_mul(pkha_reg_t dst, const pkha_number_t *a, const pkha_number_t *b,
	    const pkha_number_t *modulo)
{
	ltc_load(A0, a);
	ltc_load(B0, b);
	ltc_load(N0, modulo);

	_ltc_mod_mul(dst);
}

/* Load arguments and perform A^E mod N
 * @param dst output register, A0 or B0 are valid here
 * @param a address of value for the base
 * @param b address of value for the power
 * @param modulo address od value of modulo
 */
static __privileged __maybe_unused void
ltc_mod_exp(pkha_reg_t dst, const pkha_number_t *a, const pkha_number_t *e,
	    const pkha_number_t *modulo)
{
	ltc_load(A0, a);
	ltc_load(E, e);
	ltc_load(N0, modulo);

	_ltc_mod_exp(dst);
}

/* Load arguments and perform A mod N
 * @param dst output register, A0 or B0 are valid here
 * @param a address of value for the number
 * @param modulo address od value of modulo
 */
static __privileged void
ltc_mod_amodn(pkha_reg_t dst, const pkha_number_t *a,
	      const pkha_number_t *modulo)
{
	ltc_load(A0, a);
	ltc_load(N0, modulo);

	_ltc_mod_amodn(dst);
}

/* Load arguments and perform A^-1 mod N
 * @param dst output register, A0 or B0 are valid here
 * @param a address of value for the number
 * @param modulo address od value of modulo
 */
static __privileged __maybe_unused void
ltc_mod_inv(pkha_reg_t dst, const pkha_number_t *a, const pkha_number_t *modulo)
{
	ltc_load(A0, a);
	ltc_load(N0, modulo);

	_ltc_mod_inv(dst);
}

static __privileged __maybe_unused void
ltc_mod_r2(pkha_reg_t dst, const pkha_number_t *modulo)
{
	ltc_load(N0, modulo);

	_ltc_mod_r2(dst);
}

/* Load arguments and calculate greatest common divisor
 * @param dst output register, A0 or B0 are valid here
 * @param a address of value for the number
 * @param b address of value for the number
 */
static __privileged __maybe_unused void
ltc_mod_gcd(pkha_reg_t dst, const pkha_number_t *a, const pkha_number_t *b)
{
	ltc_load(A0, a);
	ltc_load(N0, b);

	_ltc_mod_gcd(dst);
}

static __privileged __maybe_unused void
ltc_prime_test(pkha_reg_t dst, const pkha_number_t *possible_prime,
	       const pkha_number_t *random_seed, const pkha_number_t *trials)
{
	ltc_load(A0, random_seed);
	ltc_load(B0, trials);
	ltc_load(N0, possible_prime);

	_ltc_prime_test(dst);
}

/* Load arguments and perform A + B mod N of two elliptic curve points
 * @param dst output register, A0 or B0 are valid here
 * @param point1_x address of value for the x coordinate of the first point
 * @param point1_y address of value for the y coordinate of the first point
 * @param point2_x address of value for the x coordinate of the second point
 * @param point2_y address of value for the y coordinate of the second point
 * @param curve_a address of value for the a parameter of curve equation
 * @param curve_b address of value for the b parameter of curve equation
 * @param modulo address of value of modulo
 */
static __privileged void
ltc_ecc_mod_add(pkha_reg_t dst, const pkha_number_t *point1_x,
		const pkha_number_t *point1_y, const pkha_number_t *point2_x,
		const pkha_number_t *point2_y, const pkha_number_t *curve_a,
		const pkha_number_t *curve_b, const pkha_number_t *modulo)
{
	ltc_load(A0, point1_x);
	ltc_load(A1, point1_y);
	ltc_load(B1, point2_x);
	ltc_load(B2, point2_y);
	ltc_load(A3, curve_a);
	ltc_load(B0, curve_b);
	ltc_load(N0, modulo);

	_ltc_ecc_mod_add(dst);
}

/* Load arguments and perform A + A mod N of elliptic curve point
 * @param dst output register, A0 or B0 are valid here
 * @param point_x address of value for the x coordinate of the point
 * @param point_y address of value for the y coordinate of the point
 * @param curve_a address of value for the a parameter of curve equation
 * @param curve_b address of value for the b parameter of curve equation
 * @param modulo address of value of modulo
 */
static __privileged __maybe_unused void
ltc_ecc_mod_dbl(pkha_reg_t dst, const pkha_number_t *point_x,
		const pkha_number_t *point_y, const pkha_number_t *curve_a,
		const pkha_number_t *curve_b, const pkha_number_t *modulo)
{
	ltc_load(B1, point_x);
	ltc_load(B2, point_y);
	ltc_load(A3, curve_a);
	ltc_load(B0, curve_b);
	ltc_load(N0, modulo);

	_ltc_ecc_mod_dbl(dst);
}

/* Load arguments and perform E x A mod N of elliptic curve point
 * E represents a large scalar number and A represents a point on curve.
 * @param dst output register, A0 or B0 are valid here
 * @param scalar address of value for the scalar multiplicant
 * @param point_x address of value for the x coordinate of the point
 * @param point_y address of value for the y coordinate of the point
 * @param curve_a address of value for the a parameter of curve equation
 * @param curve_b address of value for the b parameter of curve equation
 * @param modulo address of value of modulo
 */
static __privileged void
ltc_ecc_mod_mul(pkha_reg_t dst, const pkha_number_t *scalar,
		const pkha_number_t *point_x, const pkha_number_t *point_y,
		const pkha_number_t *curve_a, const pkha_number_t *curve_b,
		const pkha_number_t *modulo)
{
	ltc_load(E, scalar);
	ltc_load(A0, point_x);
	ltc_load(A1, point_y);
	ltc_load(A3, curve_a);
	ltc_load(B0, curve_b);
	ltc_load(N0, modulo);

	_ltc_ecc_mod_mul(dst);
}

/* Tell if last operation returned zero, or point at infinity.
 * @return true if the last PKHA operation result is either zero (ltc_mod_*)
 * or point at infinity (ltc_ecc_mod_*). False otherwise.
 */
static __privileged inline bool ltc_result_zero()
{
	return ltc_priv.status & LTC_STA_PKZ;
}

__privileged void *pkha_number_store_be(void *dst, const pkha_number_t *src)
{
	for (int i = PKHA_NUMBER_WORDS - 1; i >= 0; --i, dst += 4)
		put_unaligned32(__builtin_bswap32(src->words[i]), dst);

	return dst;
}

__privileged void pkha_number_load_be(pkha_number_t *dst, const void *src)
{
	for (int i = PKHA_NUMBER_WORDS - 1; i >= 0; --i, src += 4)
		dst->words[i] = __builtin_bswap32(get_unaligned32(src));
}

__privileged int pkha_number_cmp(const pkha_number_t *a, const pkha_number_t *b)
{
	for (int i = PKHA_NUMBER_WORDS - 1; i >= 0; --i) {
		if (a->words[i] < b->words[i])
			return -1;
		else if (a->words[i] > b->words[i])
			return 1;
	}

	return 0;
}

__privileged bool pkha_number_is_zero(const pkha_number_t *x)
{
	for_each(word, x->words)
		if (*word != 0)
			return false;

	return true;
}

#if DBG_ENABLE
__privileged void pkha_number_dump(const pkha_number_t *x, const char *what)
{
	debug("%s =", what);
	for (int i = PKHA_NUMBER_WORDS - 1; i >= 0; --i)
		debug(" %08x", x->words[i]);
	debug("\n");
}
#endif

_Static_assert(PKHA_NUMBER_WORDS == SHA256_CTX_WORDS,
	       "LTC ECDSA code requires PKHA numbers to be 256 bits long");

/* Load LTC SHA256 context as a number into a pkha_number_t */
static __privileged void ltc_store_from_ctx(pkha_number_t *dst)
{
	/* disable byte swap on context reads */
	BME_AND(LTC_CTL) = ~LTC_CTL_COS;

	/* copy context to E, this must be done in reverse order */
	for (unsigned i = 0; i < SHA256_CTX_WORDS; ++i)
		dst->words[i] = LTC_CTX(SHA256_CTX_WORDS - 1 - i);

	/* enable byte swap on context reads again */
	BME_OR(LTC_CTL) = LTC_CTL_COS;
}

static __privileged void ltc_ecdsa_sign_handler(enum ltc_state_e state)
{
	typeof(ltc_priv.sign) *sign = &ltc_priv.sign;
	enum ltc_state_e *next = &ltc_priv.next_state;
	const pkha_curve_t *curve = sign->curve;
	uint8_t *p = sign->buffer;

	switch (state) {
	case SIGNING_BEGIN:
		/* Start generating random number for deterministic ECDSA
		 * according to RFC 6979.
		 *
		 * We use SHA256, so hash length is 32 bytes.
		 *
		 * First set
		 *   K = 0x00 0x00 ... 0x00 (stored in sign->K)
		 *   V = 0x01 0x01 ... 0x01
		 * and compute HMAC_K(V || 0x00 || priv_key || message).
		 *
		 * The input to the HMAC is constructed in sign->buffer.
		 */

		/* K = 0x00 0x00 ... 0x00 */
		bzero(sign->K, sizeof(sign->K));

		/* V = 0x01 0x01 ... 0x01 */
		memset(p, 1, SHA256_CTX_SIZE);
		p += SHA256_CTX_SIZE;

		/* append 0x00 */
		*p++ = 0x00;

		/* append private key as integer in big endian order */
		p = pkha_number_store_be(p, sign->priv_key);

		/* append message */
		memcpy(p, sign->msg, SHA256_CTX_SIZE);

		/* compute HMAC_K(V || 0x00 || priv_key || message) */
		ltc_hmac_sha256_start(sign->K, SHA256_CTX_SIZE, sign->buffer,
				      2 * SHA256_CTX_SIZE + 1 +
				      PKHA_NUMBER_SIZE, SIGNING_K1);
		break;

	case SIGNING_K1:
		/* store K = HMAC_K(V || 0x00 || priv_key || message) */
		ltc_store_ctx_aligned(sign->K);

		/* compute HMAC_K(V) */
		ltc_hmac_sha256_start(sign->K, SHA256_CTX_SIZE, sign->buffer,
				      SHA256_CTX_SIZE, SIGNING_V1);
		break;

	case SIGNING_V1:
		/* store V = HMAC_K(V) */
		ltc_store_ctx_aligned(sign->buffer);

		/* set the byte after V in the buffer to 0x01 */
		sign->buffer[SHA256_CTX_SIZE] = 0x01;

		/* compute HMAC_K(V || 0x01 || priv_key || message) */
		ltc_hmac_sha256_start(sign->K, SHA256_CTX_SIZE, sign->buffer,
				      2 * SHA256_CTX_SIZE + 1 +
				      PKHA_NUMBER_SIZE, SIGNING_K2);
		break;

unsuitable_k:
		/* The deterministic random number computed with HMAC was found
		 * unsuitable in states below, we need to generate another one
		 * with
		 *   K = HMAC_K(V || 0x00)
		 *   V = HMAC_K(V)
		 */

		/* set the byte after V in the buffer to 0x01 */
		sign->buffer[SHA256_CTX_SIZE] = 0x00;

		/* compute HMAC_K(V || 0x00) */
		ltc_hmac_sha256_start(sign->K, SHA256_CTX_SIZE, sign->buffer,
				      SHA256_CTX_SIZE + 1, SIGNING_K2);
		break;

	case SIGNING_K2:
		/* store K either from SIGNING_V1 state as
		 *   K = HMAC_K(V || 0x01 || priv_key || message)
		 * or from unsuitable_k label as
		 *   K = HMAC_K(V || 0x00)
		 */
		ltc_store_ctx_aligned(sign->K);

		/* compute HMAC_K(V) */
		ltc_hmac_sha256_start(sign->K, SHA256_CTX_SIZE, sign->buffer,
				      SHA256_CTX_SIZE, SIGNING_V2);
		break;

	case SIGNING_V2:
		/* store V = HMAC_K(V) */
		ltc_store_ctx_aligned(sign->buffer);

		/* forget private key from the sign->buffer */
		bzero(&sign->buffer[SHA256_CTX_SIZE + 1], PKHA_NUMBER_SIZE);

		/* compute HMAC_K(V) */
		ltc_hmac_sha256_start(sign->K, SHA256_CTX_SIZE, sign->buffer,
				      SHA256_CTX_SIZE, SIGNING_RAND_MUL_G);
		break;

	case SIGNING_RAND_MUL_G:
		/* load (deterministic) random k from HMAC output
		 *
		 * NOTE: we should keep appending the deterministic random
		 *       numbers until we have enough bits:
		 *           while (tlen < qlen)
		 *               V = HMAC_K(V)
		 *               T = T || V
		 *           k = bits2int(T)
		 *       but since we assume ECDSA over 256-bit prime order and
		 *       SHA256 HMAC gives us exactly 256 bits, we can simply
		 *       do:
		 *           T = HMAC_K(V)
		 *           k = bits2int(T)
		 */
		ltc_store_from_ctx(&sign->k);

		/* clear HMAC context to forget the "random" number */
		BME_OR(LTC_CW) = LTC_CW_CCR;

		/* check if k is suitable: 0 < k < n */
		if (pkha_number_is_zero(&sign->k) ||
		    pkha_number_cmp(&sign->k, &curve->n) > 0)
			goto unsuitable_k;

		/* next state: [A0, A1] = k * G */
		ltc_ecc_mod_mul(A0, &sign->k, &curve->G.x, &curve->G.y,
				&curve->a, &curve->b, &curve->p);

		*next = SIGNING_R_MOD;
		break;

	case SIGNING_R_MOD:
		/* current state: [A0, A1] = k * G
		 *    next state: A0 = r
		 *
		 * we take r as the first coordinate of k * G modulo n, the
		 * order of G
		 */
		ltc_mod_amodn(A0, NULL, &curve->n);

		*next = SIGNING_MUL_PRIV;
		break;

	case SIGNING_MUL_PRIV:
		/* current state: A0 = r
		 *    next state: B0 = r * priv_key
		 *
		 * but first check if r is valid (non-zero)
		 */
		if (ltc_result_zero())
			goto unsuitable_k;

		/* store r, the first part of the signature */
		ltc_store(&sign->signature.r, A0);

		/* compute B0 = r * priv_key */
		ltc_mod_mul(B0, NULL, sign->priv_key, NULL);

		*next = SIGNING_Z_MOD;
		break;

	case SIGNING_Z_MOD:
		/* load message (modulo order) */
		ltc_mod_amodn(A0, &sign->z, NULL);

		*next = SIGNING_PLUS_Z;
		break;

	case SIGNING_PLUS_Z:
		/* current state: A0 = z (mod n)
		 *                B0 = r * priv_key
		 *    next state: B0 = r * priv_key + z
		 */
		_ltc_mod_add(B0);

		*next = SIGNING_TMP_SAVE;
		break;

	case SIGNING_TMP_SAVE:
		/* current state: B0 = r * priv_key + z
		 *    next state: N2 = r * priv_key + z
		 *
		 * we need to store B0 to N2 because the next operation of
		 * modular inverse has side effects on B0
		 */
		ltc_mov(N2, B0);

		*next = SIGNING_RAND_INV;
		break;

	case SIGNING_RAND_INV:
		/* load (deterministic) random k from context to A0
		 * and compute its modular inverse
		 *
		 * next state: A0 = k^-1 mod n
		 *             B0 has side effects
		 */
		ltc_mod_inv(A0, &sign->k, NULL);

		*next = SIGNING_TMP_LOAD;
		break;

	case SIGNING_TMP_LOAD:
		/* current state: A0 = k^-1 mod n
		 *                N2 = r * priv_key + z
		 *    next state: A0 = k^-1 mod n
		 *                B0 = r * priv_key + z
		 */
		ltc_mov(B0, N2);

		*next = SIGNING_S;
		break;

	case SIGNING_S:
		/* current state: A0 = k^-1 mod n
		 *                B0 = r * priv_key + z
		 *    next state: A0 = k^-1 * (r * priv_key + z)
		 */
		_ltc_mod_mul(A0);

		*next = SIGNING_CLEAR;
		break;

	case SIGNING_CLEAR:
		/* store s, the second part of the signature, but first check
		 * whether it is valid
		 */
		if (ltc_result_zero())
			goto unsuitable_k;

		ltc_store(&sign->signature.s, A0);

		/* clear all PKHA registers */
		ltc_pkha_clear_all();

		*next = SIGNING_DONE;
		break;

	case SIGNING_DONE:
		/* copy result */
		memcpy(ltc_priv.result, &sign->signature,
		       sizeof(sign->signature));

		/* forget signing private data */
		bzero(sign, sizeof(*sign));

		/* forget hmac private data */
		bzero(&ltc_priv.hmac, sizeof(ltc_priv.hmac));

		ltc_release_and_callback();

		break;

	default:
		unreachable();
	}
}

__privileged bool
ltc_ecdsa_sign(const pkha_curve_t *curve, const pkha_number_t *priv_key,
	       const void *msg, ltc_callback_t callback,
	       pkha_signature_t *result)
{
	typeof(ltc_priv.sign) *sign = &ltc_priv.sign;

	if (!ltc_try_acquire(callback, result))
		return false;

	sign->curve = curve;
	sign->priv_key = priv_key;
	sign->msg = msg;
	pkha_number_load_be(&sign->z, msg);

	ltc_ecdsa_sign_handler(SIGNING_BEGIN);

	return true;
}

static __privileged void ltc_ecdsa_verify_handler(enum ltc_state_e state)
{
	typeof(ltc_priv.verify) *verify = &ltc_priv.verify;
	const pkha_signature_t *signature = &verify->signature;
	enum ltc_state_e *next = &ltc_priv.next_state;
	const pkha_point_t *pub_key = verify->pub_key;
	const pkha_curve_t *curve = verify->curve;
	bool *result = ltc_priv.result;

	switch (state) {
	case VERIFYING_BEGIN:
		/* first validate the signature: 0 < r < n, 0 < s < n */
		if (pkha_number_is_zero(&signature->r) ||
		    pkha_number_is_zero(&signature->s) ||
		    pkha_number_cmp(&signature->r, &curve->n) != -1 ||
		    pkha_number_cmp(&signature->s, &curve->n) != -1) {
			*result = false;
			goto done;
		}

		/* next state: B0 = s^-1 (mod n) */
		ltc_mod_inv(B0, &signature->s, &curve->n);

		*next = VERIFYING_Z_MOD;
		break;

	case VERIFYING_Z_MOD:
		/* current state: B0 = s^-1 (mod n)
		 *    next state: B0 = s^-1 (mod n)
		 *                A0 = z (mod n)
		 *
		 * we need to compute message modulo order
		 */
		ltc_mod_amodn(A0, &verify->z, NULL);

		*next = VERIFYING_U1;
		break;

	case VERIFYING_U1:
		/* current state: B0 = s^-1 (mod n)
		 *                A0 = z (mod n)
		 *    next state: A0 = u1 = z * s^-1 (mod n)
		 *
		 * we also store s^-1 for later
		 */
		ltc_store(&verify->tmp, B0);

		ltc_mod_mul(A0, &verify->z, NULL, NULL);

		*next = VERIFYING_U1_MOV;
		break;

	case VERIFYING_U1_MOV:
		/* current state: A0 = u1
		 *    next state: E = u1
		 */
		ltc_mov(E, A0);

		*next = VERIFYING_U1_MUL_G;
		break;

	case VERIFYING_U1_MUL_G:
		/* current state: E = u1
		 *    next state: [A0, A1] = u1 × G
		 */
		ltc_ecc_mod_mul(A0, NULL, &curve->G.x, &curve->G.y, &curve->a,
				&curve->b, &curve->p);

		*next = VERIFYING_U2;
		break;

	case VERIFYING_U2:
		/* next state: A0 = u2 = r * s^-1 (mod n)
		 *
		 * but first store u1 × G
		 */
		ltc_store(&verify->u1g.x, A0);
		ltc_store(&verify->u1g.y, A1);

		ltc_mod_mul(A0, &signature->r, &verify->tmp, &curve->n);

		*next = VERIFYING_U2_MOV;
		break;

	case VERIFYING_U2_MOV:
		/* current state: A0 = u2
		 *    next state: E = u2
		 */
		ltc_mov(E, A0);

		*next = VERIFYING_U2_MUL_PUB_KEY;
		break;

	case VERIFYING_U2_MUL_PUB_KEY:
		/* next state: [B1, B2] = u2 × pub_key */
		ltc_ecc_mod_mul(B0, NULL, &pub_key->x, &pub_key->y, &curve->a,
				&curve->b, &curve->p);

		*next = VERIFYING_ADD;
		break;

	case VERIFYING_ADD:
		/* current state: [B1, B2] = u2 × pub_key
		 *                A3 = curve->a
		 *                B0 = curve->b
		 *                N0 = curve->p
		 *    next state: [A0, A1] = u1 × G + u2 × pub_key
		 */
		ltc_ecc_mod_add(A0, &verify->u1g.x, &verify->u1g.y, NULL, NULL,
				NULL, NULL, NULL);

		*next = VERIFYING_CLEAR;
		break;

	case VERIFYING_CLEAR:
		/* store the first coordinate of u1 × G + u2 × pub_key */
		ltc_store(&verify->tmp, A0);

		/* clear all PKHA registers */
		ltc_pkha_clear_all();

		*next = VERIFYING_COMPARE;
		break;

	case VERIFYING_COMPARE:
		/* store verification result:
		 *   the first coordinate of u1 × G + u2 × priv_key must equal r
		 */
		*result = pkha_number_cmp(&verify->tmp, &signature->r) == 0;

done:
		/* forget verifying private data */
		bzero(verify, sizeof(*verify));

		ltc_release_and_callback();

		break;

	default:
		unreachable();
	}
}

__privileged bool
ltc_ecdsa_verify(const pkha_curve_t *curve, const pkha_point_t *pub_key,
		 const void *msg, const pkha_signature_t *signature,
		 ltc_callback_t callback, bool *result)
{
	typeof(ltc_priv.verify) *verify = &ltc_priv.verify;

	if (!ltc_try_acquire(callback, result))
		return false;

	verify->curve = curve;
	verify->pub_key = pub_key;
	pkha_number_load_be(&verify->z, msg);
	memcpy(&verify->signature, signature, sizeof(*signature));

	ltc_ecdsa_verify_handler(VERIFYING_BEGIN);

	return true;
}

static __privileged void ltc_ecdsa_public_key_handler(enum ltc_state_e state)
{
	enum ltc_state_e *next = &ltc_priv.next_state;
	pkha_point_t *pub_key = ltc_priv.result;

	switch (state) {
	case PUBLIC_KEY_CLEAR:
		/* store public key coordinates */
		ltc_store(&pub_key->x, A0);
		ltc_store(&pub_key->y, A1);

		/* clear all PKHA registers */
		ltc_pkha_clear_all();

		*next = PUBLIC_KEY_DONE;
		break;

	case PUBLIC_KEY_DONE:
		*next = LTC_STATE_INVALID;
		break;

	default:
		break;
	}
}

__privileged bool
ltc_ecdsa_public_key_compressed_sync(const pkha_curve_t *curve,
				     const pkha_number_t *priv_key,
				     uint8_t result[static 1 + PKHA_NUMBER_SIZE])
{
	enum ltc_state_e *next = &ltc_priv.next_state;
	pkha_point_t pub_key;

	if (!ltc_try_acquire(NULL, &pub_key))
		return false;

	/* public key [x, y] = priv_key × G */
	*next = PUBLIC_KEY_CLEAR;
	ltc_ecc_mod_mul(A0, priv_key, &curve->G.x, &curve->G.y,
			&curve->a, &curve->b, &curve->p);

	/* Wait till ltc_ecdsa_public_key_handler() marks state as invalid,
	 * meaning that the computation is done and LTC registers are cleared.
	 */
	while (*next != LTC_STATE_INVALID)
		wait_for_interrupt();

	/* release LTC, no callback will be called */
	ltc_release_and_callback();

	/* store the public key in compressed form */
	result[0] = (pub_key.y.words[0] & 1) ? 0x03 : 0x02;
	pkha_number_store_be(&result[1], &pub_key.x);

	return true;
}

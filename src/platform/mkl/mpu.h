#ifndef MPU_H
#define MPU_H

#include "cpu.h"
#include "debug.h"

typedef enum {
	MPU_REGION_SUPERVISOR		= 0,
	MPU_REGION_HEADERS		= 1,
	MPU_REGION_TEXT			= 2,
	MPU_REGION_DATA_BSS		= 3,
	MPU_REGION_PERIPHERALS		= 4,
	MPU_REGION_FLASH		= 5,
	MPU_REGION_FLASHING_BUFFER	= 6,
	MPU_REGION_PROCESS_STACK	= 7,
} mpu_region_t;

static __privileged inline bool
_is_region_within(const void *inner, const void *inner_end,
		 const void *outer, const void *outer_end)
{
	return outer <= inner && inner_end <= outer_end;
}

static __privileged inline bool unprivileged_can_read(const void *ptr,
						      uint16_t size)
{
	extern uint32_t _stext, _etext, _sdata, _ebss,
			_sfirmwareflashdata, _efirmwareflashdata,
			_psp_bottom, _psp_top;
	const void *end = ptr + size;

	if (ptr > end)
		return false;

	return _is_region_within(ptr, end, &_stext, &_etext) ||
	       _is_region_within(ptr, end, &_sdata, &_ebss) ||
	       _is_region_within(ptr, end, (void *)0x0, (void *)0x20000) ||
	       _is_region_within(ptr, end, &_sfirmwareflashdata,
				 &_efirmwareflashdata) ||
	       _is_region_within(ptr, end, &_psp_bottom, &_psp_top);
}

static __privileged inline bool unprivileged_can_write(void *ptr, uint16_t size)
{
	extern uint32_t _sdata, _ebss, _psp_bottom, _psp_top;
	void *end = ptr + size;

	if (ptr > end)
		return false;

	return _is_region_within(ptr, end, &_sdata, &_ebss) ||
	       _is_region_within(ptr, end, &_psp_bottom, &_psp_top);
}

static __privileged inline void
assert_unprivileged_can_read(const void *ptr, uint16_t size, const char *msg)
{
	void hardfault_handler(void);

	if (unprivileged_can_read(ptr, size))
		return;

	debug("%s: unprivileged memory read to %p\n", msg, ptr);

	hardfault_handler();
}

static __privileged inline void
assert_unprivileged_can_write(void *ptr, uint16_t size, const char *msg)
{
	void hardfault_handler(void);

	if (unprivileged_can_write(ptr, size))
		return;

	debug("%s: unprivileged memory write to %p\n", msg, ptr);

	hardfault_handler();
}

#endif /* MPU_H */

#include "crypto.h"
#include "ltc.h"
#include "string.h"
#include "memory_layout.h"
#include "trng.h"
#include "flash_plat.h"
#include "led_driver.h"
#include "reset.h"
#include "debug.h"

#ifndef CRYPTO_TEST
#error build system did not define CRYPTO_TEST macro
#endif

/* If the PRODUCTION_BUILD macro is set to 0, the firmware signature
 * verification procedure will use the development ECDSA key to verify the
 * firmware. Otherwise the production key will be used.
 */
#ifndef PRODUCTION_BUILD
#error build system did not define PRODUCTION_BUILD macro
#endif

/* The NIST256p elliptic curve definition */
static const pkha_curve_t nist256p = {
	.G = {
		.x = { .words = { 0xd898c296, 0xf4a13945, 0x2deb33a0,
				  0x77037d81, 0x63a440f2, 0xf8bce6e5,
				  0xe12c4247, 0x6b17d1f2 }},
		.y = { .words = { 0x37bf51f5, 0xcbb64068, 0x6b315ece,
				  0x2bce3357, 0x7c0f9e16, 0x8ee7eb4a,
				  0xfe1a7f9b, 0x4fe342e2 }},
	},
	.n = { .words = { 0xfc632551, 0xf3b9cac2, 0xa7179e84, 0xbce6faad,
			  0xffffffff, 0xffffffff, 0x00000000, 0xffffffff }},
	.a = { .words = { 0xfffffffc, 0xffffffff, 0xffffffff, 0x00000000,
			  0x00000000, 0x00000000, 0x00000001, 0xffffffff }},
	.b = { .words = { 0x27d2604b, 0x3bce3c3e, 0xcc53b0f6, 0x651d06b0,
			  0x769886bc, 0xb3ebbd55, 0xaa3a93e7, 0x5ac635d8 }},
	.p = { .words = { 0xffffffff, 0xffffffff, 0xffffffff, 0x00000000,
			  0x00000000, 0x00000000, 0x00000001, 0xffffffff }}
};

#if PRODUCTION_BUILD
static const pkha_point_t fw_signing_pub_key = {
	.x = { .words = { 0x65f067d5, 0xcdfdbad5, 0xa74a761e, 0x1d33ad58,
			  0x830cc11f, 0xaecf1656, 0xc7517ca0, 0x121eb29d }},
	.y = { .words = { 0x802bae98, 0x7564fabf, 0x4a588919, 0x475fad3f,
			  0x168940ef, 0xd573c0f8, 0xc9969db0, 0xf8a18849 }},
};
#else
/* For development build the firmware is signed with a development ECDSA key.
 * This is the same key as the one used below used below in test_ecdsa_sign()
 * and test_ecdsa_verify(), the private key comes from RFC 6979 test vectors.
 */
static const pkha_point_t fw_signing_pub_key = {
	.x = { .words = { 0x60f29fb6, 0xe669622e, 0x3b61fa6c, 0xc049b892,
			  0xc6356d68, 0xc961eb74, 0x255a9d31, 0x60fed4ba }},
	.y = { .words = { 0xd4462299, 0x77a3c294, 0x2d7e9f51, 0xf2f1b20c,
			  0x5628bc64, 0xa41ae9e9, 0x08b8bc99, 0x7903fe10 }},
};
#endif

/* Private data for firmware verification */
static struct {
	bool busy; /* true if the fwverify structure is being used */

	/* pointer to firmware signature within the firmware */
	const firmware_signature_t *fwsig;

	uint8_t hash[SHA256_CTX_SIZE]; /* firmware hash */
	bool verify_result; /* signature verification result */

	verify_callback_t callback;
	void *priv;
} fwverify __privileged_data;

/** Acquire the fwverify structure */
static __privileged bool
fwverify_acquire(verify_callback_t callback, void *priv)
{
	bool res;

	disable_irq();

	res = !fwverify.busy;
	if (res) {
		fwverify.busy = true;
		fwverify.callback = callback;
		fwverify.priv = priv;
	}

	enable_irq();

	return res;
}

/** Release the fwverify structure and call the callback */
static __privileged void fwverify_release_and_callback(bool result)
{
	verify_callback_t callback = fwverify.callback;
	void *priv = fwverify.priv;

	/* forget the verification (this also releases the strucutre) */
	bzero(&fwverify, sizeof(fwverify));

	/* call back */
	callback(result, priv);
}

/** Called when firmware signature verification is done */
static __privileged void fw_verify_cb(void *result)
{
	if (unlikely(!result)) {
		debug("error verifying firmware signature\n");

		return fwverify_release_and_callback(false);
	}

	fwverify_release_and_callback(fwverify.verify_result);
}

/** Called when firmware sha256 computation is done */
static __privileged void fw_hash_cb(void *result)
{
	const firmware_signature_t *fwsig = fwverify.fwsig;

	if (unlikely(!result)) {
		debug("error hashing firmware\n");

		return fwverify_release_and_callback(false);
	}

	/* print sha256 checksum */
	debug("firmware sha256: ");
	for_each(byte, fwverify.hash)
		debug("%02x", *byte);
	debug("\n");

	/* check sha256 checksum */
	if (memcmp(fwsig->hash, fwverify.hash, SHA256_CTX_SIZE)) {
		debug("firmware has invalid sha256, expected: ");
		for_each(byte, fwsig->hash)
			debug("%02x", *byte);
		debug("\n");

		return fwverify_release_and_callback(false);
	}

	/* start signature verification */
	if (!ltc_ecdsa_verify(&nist256p, &fw_signing_pub_key, fwverify.hash,
			      &fwsig->signature, fw_verify_cb,
			      &fwverify.verify_result)) {
		debug("could not verify firmware signature\n");

		return fwverify_release_and_callback(false);
	}
}

__privileged void
crypto_verify_firmware_signature(const void *firmware, uint16_t size,
				 verify_callback_t callback, void *priv)
{
	/* try to acquire firmware verification private structure */
	if (!fwverify_acquire(callback, priv)) {
		debug("firmware verification busy\n");
		callback(false, priv);
		return;
	}

	/* sanity check if the size is enough to contain the signature */
	if (size < sizeof(firmware_signature_t)) {
		debug("firmware is too short to contain signature structure\n");
		return fwverify_release_and_callback(false);
	}

	/* we only want to hash the firmware without signature */
	size -= sizeof(firmware_signature_t);

	/* remember pointer to the firmwaresignature structure */
	fwverify.fwsig = firmware + size;

	/* compute sha256 of the firmware */
	if (!ltc_sha256_async(fwverify.hash, firmware, size, fw_hash_cb)) {
		debug("could not hash firmware\n");
		return fwverify_release_and_callback(false);
	}
}

#if BOOTLOADER_BUILD
typedef struct {
	bool result;
	bool done;
} fwverify_sync_t;

/** Callback for synchronous firmware signature verification */
static __privileged void fwverify_sync_cb(bool result, void *priv)
{
	fwverify_sync_t *op = priv;

	op->result = result;
	op->done = true;
}

__privileged bool crypto_verify_application(void)
{
	fwverify_sync_t op;
	uint32_t size;

	size = *(uint32_t *)APPLICATION_CRCSUM;

	/* sanity check */
	if (!size || size > APPLICATION_MAX_SIZE || size % 4) {
		debug("Invalid length stored in application checksum!\n");
		return false;
	}

	op.done = false;
	crypto_verify_firmware_signature((const void *)APPLICATION_BEGIN, size,
					 fwverify_sync_cb, &op);

	while (!op.done)
		wait_for_interrupt();

	return op.result;
}
#endif

#if CRYPTO_TEST && !BOOTLOADER_BUILD

/* Test vectors from RFC 6979 */

/* Private key */
static const pkha_number_t test_priv_key = {
	.words = { 0x120f6721, 0x7b8a622b, 0x36e89b12, 0x4e50c3db,
		   0x67b1d693, 0x6b5c2157, 0x45ba7516, 0xc9afa9d8 }
};

/* Public key */
static const pkha_point_t test_pub_key = {
	.x = { .words = { 0x60f29fb6, 0xe669622e, 0x3b61fa6c, 0xc049b892,
			  0xc6356d68, 0xc961eb74, 0x255a9d31, 0x60fed4ba }},
	.y = { .words = { 0xd4462299, 0x77a3c294, 0x2d7e9f51, 0xf2f1b20c,
			  0x5628bc64, 0xa41ae9e9, 0x08b8bc99, 0x7903fe10 }},
};

/* Signature of sha256("sample") with the above private key */
static const pkha_signature_t test_signature = {
	.r = { .words = { 0x4eaf3716, 0xc34d0ea8, 0x56aaf991, 0x9d2c877b,
			  0xd45e81d6, 0x1140dd9c, 0xacb6a8fd, 0xefd48b2a }},
	.s = { .words = { 0x843acda8, 0x4dc4ab2f, 0xb9aff406, 0xf3e900db,
			  0xb6e29f65, 0xd436c7a1, 0x2d657c41, 0xf7cb1c94 }},
};

/* sha256 of "sample" */
static const uint8_t test_hash[32] = {
	0xaf, 0x2b, 0xdb, 0xe1, 0xaa, 0x9b, 0x6e, 0xc1,
	0xe2, 0xad, 0xe1, 0xd6, 0x94, 0xf4, 0x1f, 0xc7,
	0x1a, 0x83, 0x1d, 0x02, 0x68, 0xe9, 0x89, 0x15,
	0x62, 0x11, 0x3d, 0x8a, 0x62, 0xad, 0xd1, 0xbf,
};

typedef struct {
	pkha_signature_t signature;
	bool done;
} ecdsa_sign_sync_t;

/** Callback for ltc_ecdsa_sign that marks the operation as done */
static __privileged void ecdsa_sign_sync_cb(void *ptr)
{
	ecdsa_sign_sync_t *result = ptr;

	result->done = true;
}

typedef struct {
	bool result;
	bool done;
} ecdsa_verify_sync_t;

/** Callback for ltc_ecdsa_verify that marks the operation as done */
static __privileged void ecdsa_verify_sync_cb(void *ptr)
{
	ecdsa_verify_sync_t *result = ptr;

	result->done = true;
}

/** Test sha256 on a sample input */
static __privileged void test_crypto_sha256(void)
{
	uint8_t hash[SHA256_CTX_SIZE];

	debug("testing sha256(\"sample\"): ");
	if (!ltc_sha256(hash, "sample", 6)) {
		debug("failed, LTC is busy\n");
		return;
	}

	if (memcmp(hash, test_hash, SHA256_CTX_SIZE)) {
		debug("failed, got digest\n    ");
		for_each(byte, hash)
			debug("%02x", *byte);
		debug("\n  but expected digest\n    ");
		for_each(byte, test_hash)
			debug("%02x", *byte);
		return;
	}

	for_each(byte, hash)
		debug("%02x", *byte);
	debug("\n");
}

/** Test ECDSA signing */
static __privileged void test_ecdsa_sign(void)
{
	ecdsa_sign_sync_t op;

	debug("testing ECDSA signing: ");

	op.done = false;
	if (!ltc_ecdsa_sign(&nist256p, &test_priv_key, test_hash,
			    ecdsa_sign_sync_cb, (void *)&op)) {
		debug("failed, LTC is busy\n");
		return;
	}

	while (!op.done)
		wait_for_interrupt();

	if (pkha_number_cmp(&op.signature.r, &test_signature.r) ||
	    pkha_number_cmp(&op.signature.s, &test_signature.s)) {
		debug("failed, got signature\n");
		pkha_number_dump(&op.signature.r, "    r");
		pkha_number_dump(&op.signature.s, "    s");
		debug("  but expected\n");
		pkha_number_dump(&test_signature.r, "    r");
		pkha_number_dump(&test_signature.s, "    s");
	} else {
		debug("OK, got signature\n");
		pkha_number_dump(&op.signature.r, "  r");
		pkha_number_dump(&op.signature.s, "  s");
	}
}

/** Test ECDSA verification
 * @param expected_result if true, run a test where the verification should
 *        succeed, if false, run a test where the verification should fail
 */
static __privileged void test_ecdsa_verify(bool expected_result)
{
	pkha_signature_t signature = test_signature;
	ecdsa_verify_sync_t op;

	debug("testing ECDSA verification%s: ",
	      !expected_result ? " that should fail" : "");

	if (!expected_result)
		signature.s.words[0] += 1;

	op.done = false;
	if (!ltc_ecdsa_verify(&nist256p, &test_pub_key, test_hash, &signature,
			      ecdsa_verify_sync_cb, (void *)&op)) {
		debug("failed, LTC is busy\n");
		return;
	}

	while (!op.done)
		wait_for_interrupt();

	debug("%s%s\n", op.result ? "verified": "failed",
	      op.result && !expected_result ? ", but should FAIL" : "");
}

/** Run the tests */
static __privileged void crypto_test(void)
{
	test_crypto_sha256();
	test_ecdsa_sign();
	test_ecdsa_verify(true);
	test_ecdsa_verify(false);
}

#else /* !(CRYPTO_TEST && !BOOTLOADER_BUILD) */
static __privileged inline void crypto_test(void)
{
}
#endif /* !(CRYPTO_TEST && !BOOTLOADER_BUILD) */

#if !BOOTLOADER_BUILD

/* board ECDSA private key, must reside in privileged area */
static pkha_number_t board_private_key __privileged_data;

/* board ECDSA key validity indicator */
static bool board_key_valid __privileged_data;

/* board ECDSA public key, compressed */
static uint8_t board_public_key[1 + PKHA_NUMBER_SIZE] __privileged_data;

static struct {
	bool busy, done, error;
	pkha_signature_t signature;
} sign __privileged_data;

/** Callback for when message signing with board private key is done */
static __privileged void board_sign_done_cb(void *result)
{
	/* set done and error flags */
	sign.done = true;
	sign.error = !result;
}

__privileged bool
crypto_sign_message(const uint8_t msg[static SHA256_CTX_SIZE])
{
	bool ret = false;

	disable_irq();

	/* if signing is in process, return failure */
	if (sign.busy)
		goto end;

	/* unset done flag and start signing */
	sign.done = false;
	if (!ltc_ecdsa_sign(&nist256p, &board_private_key, msg,
			    board_sign_done_cb, &sign.signature))
		goto end;

	/* set busy flag and return success */
	sign.busy = true;
	ret = true;

end:
	enable_irq();

	return ret;
}

__privileged bool crypto_signature_done(void)
{
	return sign.done;
}

__privileged bool
crypto_signature_collect(uint8_t signature[static 2 * PKHA_NUMBER_SIZE])
{
	uint8_t *ptr = signature;
	bool ret = false;

	disable_irq();

	/* if no signing is in process or it is not done, return failure */
	if (!sign.busy || !sign.done)
		goto end;

	/* if an error occurred, skip storing signature */
	if (sign.error)
		goto error;

	/* store the signature in big endian order */
	ptr = pkha_number_store_be(ptr, &sign.signature.r);
	pkha_number_store_be(ptr, &sign.signature.s);

	/* return success */
	ret = true;

error:
	/* clear structure for security (also unsets flags) */
	bzero(&sign, sizeof(sign));

end:
	enable_irq();

	return ret;
}

__privileged bool
crypto_get_public_key(uint8_t public_key[static 1 + PKHA_NUMBER_SIZE])
{
	if (board_key_valid)
		memcpy(public_key, board_public_key, sizeof(board_public_key));

	return board_key_valid;
}

static __privileged bool read_private_key(bool *is_generated)
{
	bool lower_half_all_ones = true, upper_half_all_zeros = true;
	uint32_t *pk = &board_private_key.words[0];

	/* Read the lower 16 bytes of the private key from the Program Once
	 * Field at indexes 0x30 - 0x33, which is readable with the Read Once
	 * flash command.
	 * If these values all read 0xffffffff, we interpret this state as such
	 * that the private key was not yet generated for this board.
	 */
	for (unsigned i = 0; i < 4; ++i) {
		pk[i] = flash_read_once_sync(0x30 + i);
		if (pk[i] != 0xffffffff)
			lower_half_all_ones = false;
	}

	/* the private key was not generated yet for this board */
	if (lower_half_all_ones) {
		*is_generated = false;
		return false;
	}

	*is_generated = true;

	/* Read the upper 16 bytes of the private key from the Program Once
	 * Field at indexes 0x20 - 0x23, which is only readable from the
	 * SIM_SECKEYn registers, and only if the flash is in secure state.
	 *
	 * The SIM_SECKEYn registers can only be read once, and subsequent reads
	 * will return 0.
	 *
	 * If the flash is in unsecure state, SIM_SECKEYn read as 0. Since this
	 * code can be reached in unsecure state only if this is a development
	 * build, we will read a different private key for development build
	 * than what was generated. But this is not an issue, since development
	 * builds are only used during development. Reading all zeros on the
	 * upper half of the private key also ensures that the key is valid for
	 * NIST256p ECDSA (since it is less than the curve order).
	 *
	 * If somehow we get here reading all zeros while the flash is in secure
	 * state, it means that the SIM_SECKEYn registers have already been
	 * read once. In that case the private key is invalid.
	 */
	for (unsigned i = 0; i < 4; ++i) {
		pk[4 + i] = SIM_SECKEY(i);
		if (pk[4 + i])
			upper_half_all_zeros = false;
	}

	if (!flash_is_secure())
		return true;

	return !upper_half_all_zeros;
}

static __privileged bool program_private_key(const pkha_number_t *x)
{
	for (unsigned i = 0; i < 4; ++i)
		if (!flash_program_once_sync(0x30 + i, x->words[i]))
			return false;

	/* The documentation says that SIM_SECKEYn correspons to Program Once
	 * Field at index (0x20 + n), but in reality the order is reversed, so
	 * SIM_SECKEYn corresponds to index (0x23 - n).
	 */
	for (unsigned i = 0; i < 4; ++i)
		if (!flash_program_once_sync(0x23 - i, x->words[4 + i]))
			return false;

	return true;
}

static __privileged bool is_private_key_valid(const pkha_number_t *x)
{
	bool lower_half_all_ones = true, upper_half_all_zeros = true;

	/* private key must not be zero */
	if (pkha_number_is_zero(x))
		return false;

	/* private key must be less than curve order */
	if (pkha_number_cmp(x, &nist256p.n) >= 0)
		return false;

	/* the lower half must not be all ones, otherwise the read_private_key()
	 * function will not work correctly
	 */
	for (unsigned i = 0; i < 4; ++i)
		if (x->words[i] != 0xffffffff)
			lower_half_all_ones = false;

	if (lower_half_all_ones)
		return false;

	/* the upper half must not be all zeros, otherwise the
	 * read_private_key() function will not work correctly
	 */

	for (unsigned i = 4; i < 8; ++i)
		if (x->words[i])
			upper_half_all_zeros = false;

	if (upper_half_all_zeros)
		return false;

	return true;
}

static __privileged bool generate_private_key(void)
{
	struct {
		uint32_t entropy[16];
		uint8_t hash[SHA256_CTX_SIZE];
	} buf = {};

	debug("Generating new private key for the board...");

	do {
		static const unsigned private_key_entropy_bits =
			PRODUCTION_BUILD ? 2048 : 512;

		/* For each iteration, collect 512 bits of entropy and and
		 * another 256 bits of previous hash and hash this into new 256
		 * bits of hash. (The previous hash is all zeros at the first
		 * iteration.)
		 * Repeat until enough bits of entropy has been collected, and
		 * use the last hash as the private key.
		 */
		for (unsigned i = 0; i < private_key_entropy_bits; i += 512) {
			trng_collect_entropy_sync(buf.entropy);
			if (!ltc_sha256(buf.hash, buf.entropy, sizeof(buf))) {
				debug(" failed, LTC is busy\n");
				return false;
			}
			debug(".");
		}

		_Static_assert(PKHA_NUMBER_SIZE == SHA256_CTX_SIZE,
			       "crypto code requires PKHA numbers to be 256 bits long");

		memcpy(board_private_key.words, buf.hash, PKHA_NUMBER_SIZE);
	} while (!is_private_key_valid(&board_private_key));

	if (!program_private_key(&board_private_key)) {
		debug(" failed programming\n");
		return false;
	}

	debug(" done\n");

	/* If the flash is in unsecure state (can only happen in development)
	 * zero out the upper half of the private key stored in RAM, so that
	 * we will have the same private key as read_private_key() will return
	 * in unsecure state (upper half all zeros).
	 */
	if (!flash_is_secure())
		for (unsigned i = 4; i < 8; ++i)
			board_private_key.words[i] = 0;

	return true;
}

static __privileged void board_ecdsa_key_init(void)
{
	bool is_generated;

	/* read private key */
	board_key_valid = read_private_key(&is_generated);
	if (!board_key_valid) {
		if (is_generated) {
			debug("Board has invalid private key, MCU flash mass erase is needed\n");
			return;
		}

		/* private key was not generated yet, generate one */
		board_key_valid = generate_private_key();
		if (!board_key_valid) {
			debug("Failed to generate valid private key for the board\n");
			return;
		}
	}

	/* compute public key */
	if (!ltc_ecdsa_public_key_compressed_sync(&nist256p, &board_private_key,
						  board_public_key)) {
		debug("Failed to compute board public key, LTC is busy\n");
		board_key_valid = false;
		return;
	}

	debug("Board public key: ");
	for_each(byte, board_public_key)
		debug("%02x", *byte);
	debug("\n");
}

#else /* BOOTLOADER_BUILD */
static __privileged void board_ecdsa_key_init(void)
{
}
#endif /* BOOTLOADER_BUILD */

__privileged void crypto_init(void)
{
	/* In bootloader build we also secure the flash so that debug port is
	 * disabled and there can be no unauthorized access to the board private
	 * key.
	 */
	if (BOOTLOADER_BUILD && PRODUCTION_BUILD && !flash_is_secure()) {
		if (flash_set_secure_sync()) {
			debug("\n\nFlash set to secure state, resetting\n\n");
			hard_reset();
		}

		debug("Could not set flash into secure state\n");

		/* blink every other LED red to indicate this error */
		led_set_color24(LED_COUNT, RED_COLOR);
		led_driver_blink_pattern_start(0b101010101010, 0, 500, 0,
					       false);

		/* loop */
		while (true)
			wait_for_interrupt();
	}

	ltc_init();

	crypto_test();

	if (!BOOTLOADER_BUILD)
		board_ecdsa_key_init();
}

__privileged void crypto_reset(void)
{
	ltc_reset();

	/*
	 * If fwverify was in progress, fwverify_release_and_callback() was also
	 * called from ltc_reset(), which cleared the fwverify structure. No
	 * need to clear it from here again.
	 */

#if !BOOTLOADER_BUILD
	/*
	 * If signature generation with board private key was in progress, the
	 * board_sign_done_cb() callback was called from ltc_reset(), but it
	 * does not clear the sign structure, since it needs to report an error
	 * in the case when a LTC error occurs. But now we do want to clear it.
	 */
	bzero(&sign, sizeof(sign));
#endif
}

#ifndef TRNG_PLAT_H
#define TRNG_PLAT_H

#include "cpu.h"
#include "bitband.h"
#include "gd32f1x0_adc.h"
#include "gd32f1x0_dma.h"
#include "gd32f1x0_rcu.h"

#ifndef TRNG_PRIV_COMMON_MEMBERS
#error do not include trng_plat.h, it is meant to be included only from trng_common_stm32_gd32.c
#endif

static struct {
	TRNG_PRIV_COMMON_MEMBERS

	/* for bit-banding the entropy buffer */
	uint32_t *start_bit, *end_bit, *cur_bit;
} trng;

/* we use bit-banding of the adc_dma_buffer */
#define trng_for_each_bit_pair(_b0, _b1)				\
	uint32_t *_b0 = &BITBAND_R(trng.adc_dma_buffer[0], BIT(0));	\
	uint32_t *_b1 = _b0 + 32;					\
	for (unsigned _i = 0; _i < ARRAY_SIZE(trng.adc_dma_buffer);	\
	     _i += 2, _b0 += 64, _b1 += 64)

static inline bool trng_bits_same(uint32_t *bit0, uint32_t *bit1)
{
	/* inform the compiler that the pointers are to bit-band area */
	if (*bit0 > 1 || *bit1 > 1)
		unreachable();

	return *bit0 == *bit1;
}

static inline bool trng_entropy_emit_bit(uint32_t *bit)
{
	/* inform the compiler that the pointer is to bit-band area */
	if (*bit > 1)
		unreachable();

	*trng.cur_bit++ = *bit;

	return *bit;
}

static __force_inline bool trng_entropy_full(void)
{
	return trng.cur_bit == trng.end_bit;
}

static inline void trng_dma_reinit(void)
{
	/*
	 * Disable DMA channel so that we can reinitialize count.
	 * Do not enable it again. On GD32 it would make ADC continue sampling.
	 */
	BITBAND_P(DMA_CH0CTL, DMA_CHXCTL_CHEN) = 0;
	DMA_CH0CNT = ARRAY_SIZE(trng.adc_dma_buffer);
}

static inline void trng_reset_gathering(void)
{
	/* reset bit-banding current bit to first bit */
	trng.cur_bit = trng.start_bit;
}

static inline void trng_adc_start(void)
{
	/* enable DMA channel, this starts ADC sampling */
	BITBAND_P(DMA_CH0CTL, DMA_CHXCTL_CHEN) = 1;
}

static inline bool trng_dma_is_error(void)
{
	return BITBAND_P(DMA_INTF, DMA_INTF_ERRIF);
}

static inline void trng_dma_irq_clear(void)
{
	DMA_INTC = DMA_INTF;
}

static inline void trng_adc_init(void)
{
	/* initialize bit-banding */
	trng.start_bit = &BITBAND_R(trng.entropy[0], BIT(0));
	trng.end_bit = &BITBAND_R(trng.entropy[ARRAY_SIZE(trng.entropy)],
				  BIT(0));

	/* enable DMA clock */
	BITBAND_P(RCU_AHBEN, RCU_AHBEN_DMAEN) = 1;

	/* initialize DMA channel 0 to fill trng.adc_dma_buffer */
	DMA_CH0PADDR = (uint32_t)&ADC_RDATA;
	DMA_CH0MADDR = (uint32_t)&trng.adc_dma_buffer;
	DMA_CH0CNT = ARRAY_SIZE(trng.adc_dma_buffer);
	DMA_CH0CTL = DMA_CHXCTL_FTFIE | DMA_CHXCTL_ERRIE | DMA_CHXCTL_MNAGA |
		     DMA_PERIPHERAL_WIDTH_32BIT | DMA_MEMORY_WIDTH_32BIT;

	/* enable DMA irq */
	nvic_enable_irq_with_prio(DMA_Channel0_IRQn, 3);

	/* configure ADC clock to APB2 / 8 */
	BITBAND_P(RCU_CFG2, RCU_CFG2_ADCSEL) = 1;
	RCU_CFG0 = (RCU_CFG0 & ~RCU_CFG0_ADCPSC) | RCU_ADC_CKAPB2_DIV8;

	/* reset ADC and enable ADC clock */
	BITBAND_P(RCU_APB2RST, RCU_APB2RST_ADCRST) = 1;
	BITBAND_P(RCU_APB2RST, RCU_APB2RST_ADCRST) = 0;
	BITBAND_P(RCU_APB2EN, RCU_APB2EN_ADCEN) = 1;

	/* enable ADC */
	BITBAND_P(ADC_CTL1, ADC_CTL1_ADCON) = 1;
	for (unsigned i = 0; i < 16; ++i)
		nop();

	/* ADC calibration reset */
	BITBAND_P(ADC_CTL1, ADC_CTL1_RSTCLB) = 1;
	while (BITBAND_P(ADC_CTL1, ADC_CTL1_RSTCLB))
		nop();

	/* ADC calibration */
	BITBAND_P(ADC_CTL1, ADC_CTL1_CLB) = 1;
	while (BITBAND_P(ADC_CTL1, ADC_CTL1_CLB))
		nop();

	/*
	 * enable external regular trigger and set it to timer 1 channel 1,
	 * enable temperature sensor, enable DMA requests
	 */
	ADC_CTL1 = (ADC_CTL1 & ~ADC_CTL1_ETSRC) | ADC_CTL1_ETERC |
		   ADC_EXTTRIG_REGULAR_T2_TRGO | ADC_CTL1_TSVREN | ADC_CTL1_DMA;

	/* set regular sequence length to 1 */
	ADC_RSQ0 = FIELD_PREP(ADC_RSQ0_RL, 0);
	/* set regular sequence to list only the temperature sensor channel */
	ADC_RSQ2 = 0x10;
	/* set temperature sensor channel sampling 239.5 clock cycles */
	ADC_SAMPT0 = 0x7 << 18;
}

static inline void trng_adc_disable(void)
{
	/* disable DMA channel */
	BITBAND_P(DMA_CH0CTL, DMA_CHXCTL_CHEN) = 0;

	/* disable ADC */
	BITBAND_P(ADC_CTL1, ADC_CTL1_ADCON) = 0;

	/* disable ADC clock and reset ADC */
	BITBAND_P(RCU_APB2EN, RCU_APB2EN_ADCEN) = 0;
	BITBAND_P(RCU_APB2RST, RCU_APB2RST_ADCRST) = 1;
	BITBAND_P(RCU_APB2RST, RCU_APB2RST_ADCRST) = 0;

	/* reset DMA channel and IRQs*/
	DMA_INTC = DMA_INTF;
	DMA_CH0CTL = 0;
	DMA_CH0CNT = 0;
	DMA_CH0PADDR = 0;
	DMA_CH0MADDR = 0;
	nvic_clear_pending(DMA_Channel0_IRQn);

	/* disable DMA clock */
	BITBAND_P(RCU_AHBEN, RCU_AHBEN_DMAEN) = 0;
}

#endif /* TRNG_PLAT_H */

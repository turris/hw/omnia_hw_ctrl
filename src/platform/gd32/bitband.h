#ifndef BITBAND_H
#define BITBAND_H

#include "cpu.h"

#define __BB(_a, _base, _bb_base) \
	((_bb_base) + ((_a) - (_base)) * 32)
#define _BB(_a, _b, _base, _bb_base) \
	(__BB((uint32_t)&(_a), _base, _bb_base) + 4 * __bf_shf(_b))

#define BITBAND_R(_a, _b) \
	(*(uint32_t *)_BB(_a, _b, SRAM_BASE, SRAM_BB_BASE))
#define BITBAND_P(_a, _b) \
	(*(volatile uint32_t *)_BB(_a, _b, APB1_BUS_BASE, PERIPH_BB_BASE))

#endif /* BITBAND_H */

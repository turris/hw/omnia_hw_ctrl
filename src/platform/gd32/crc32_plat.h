#ifndef CRC32_PLAT_H
#define CRC32_PLAT_H

#include "gd32f1x0_rcu.h"
#include "gd32f1x0_crc.h"
#include "compiler.h"
#include "cpu.h"

#define CRC_FREE_DATA_REG	CRC_FDATA

uint32_t crc32_plat(uint32_t init, const void *src, uint16_t len);

static inline void crc32_enable(void)
{
	RCU_AHBEN |= RCU_AHBEN_CRCEN;
}

#endif /* CRC32_PLAT_H */

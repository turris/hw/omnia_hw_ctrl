#ifndef __LED_DRIVER_H
#define __LED_DRIVER_H

#include "cpu.h"
#include "svc.h"

#define LED_COUNT	12

enum colors {
	WHITE_COLOR	= 0xFFFFFF,
	RED_COLOR	= 0xFF0000,
	GREEN_COLOR	= 0x00FF00,
	BLUE_COLOR	= 0x0000FF,
	BLACK_COLOR	= 0x000000,
	YELLOW_COLOR	= 0xFFFF00,
};

enum led_names {
	POWER_LED	= 11,
	LAN0_LED	= 10,
	LAN1_LED	= 9,
	LAN2_LED	= 8,
	LAN3_LED	= 7,
	LAN4_LED	= 6,
	WAN_LED		= 5,
	PCI1_LED	= 3,
	PCI2_LED	= 4,
	MSATA_PCI_LED	= 2,
	USER_LED1	= 1,
	USER_LED2	= 0,
};

typedef struct {
	union {
		struct {
			uint8_t b, g, r, _;
		};
		uint32_t rgb;
	};
} rgb_t;

#define LED_MASK_ALL	GENMASK(11, 0)

static inline uint16_t led_bits(unsigned led)
{
	if (led >= LED_COUNT)
		return LED_MASK_ALL;
	else
		return BIT(led);
}

/* PCI1 and PCI2 leds are reversed, there is a difference between numbering in schematic
editor and numbering on the case for the router */

/*******************************************************************************
  * @function   led_driver_config
  * @brief      Configure LED driver.
  * @param      None.
  * @retval     None.
  *****************************************************************************/
void led_driver_config(void);

void led_driver_init(void);

void led_set_color(unsigned led, uint8_t r, uint8_t g, uint8_t b);

/*******************************************************************************
  * @function   led_set_color24
  * @brief      Save color of LED specified in parameters to be displayed in next cycle.
  * @param      led: position of LED (0..11) or led >=12 -> all LEDs
  * @param      color: LED color (24-bit RGB).
  * @retval     None.
  *****************************************************************************/
static inline void led_set_color24(unsigned led, uint32_t color)
{
	rgb_t rgb = { .rgb = color };

	led_set_color(led, rgb.r, rgb.g, rgb.b);
}

void led_driver_set_gamma_correction(bool on);
bool led_driver_get_gamma_correction(void);

/** Set per-LED corrections.
 * With gamma correction enabled, the PWM duty of a LED channel is computed as
 *   pwm_duty = (gamma_table[channel_value] * correction + rounding) / 4096.
 * The correction variable can be set for each channel of each LED. The rounding
 * variable is common for all LEDs and all channels.
 *
 * This function allows to set the correction and rounding variables to
 * non-default values. This can be useful if some LEDs have become a little bit
 * dimmer over years of usage.
 *
 * Example:
 * - The default value of the correction variable on GD32 is 1926 for the green
 *   channel for all LEDs, the default value of rouding variable is 2175.
 * - If gamma correction is enabled and user sets RGB color of a LED to #ff8000
 *   (orange color), we have
 *
 *     pwm_duty = (gamma_table[channel_value] * correction + rounding) / 4096
 *              = (gamma_table[0x80]          * 1926       + 2175    ) / 4096
 *              = (190 * 1926 + 2175) / 4096
 *              = 89.
 *
 *   So the PWM duty of the green channel of that LED will be 89 / 1023 (1023 is
 *   the maximum (full) PWM duty on GD32 when gamma correction is enabled).
 * - Now imagine that the blue channel of LED 4 is dimmer than the rest, when
 *   set to full brightness. To equalize this, we have set the correction value
 *   of all the blue channel of all the other LEDs to a lower value. Finding
 *   the correct value must be done via experimentation. After that the
 *   correction values must also be found and updated for red and green channels
 *   in order for the colors to mix properly.
 *
 * @param corr an array of 37 values: the first one is the rounding value, and
 *        the rest 36 values correspond to correction values for red, green and
 *        blue channels for all LEDs. The array goes as
 *        (rounding, correction_led0_r, correction_led0_g, correction_led0_b,
 *         correction_led1_r, correction_led1_g, correction_led1_b, ...).
 *        The rounding value has a maximum of 4095. The correction values have a
 *        maximum of 4096 and must not be zero. If an invalid value is given,
 *        the default value will be set.
 */
void led_driver_set_corrections(const uint16_t corr[static 1 + LED_COUNT * 3]);

/** Get per-LED corrections. */
void led_driver_get_corrections(uint16_t corr[static 1 + LED_COUNT * 3]);

/*******************************************************************************
  * @function   led_driver_irq_handler
  * @brief      Send RGB LEDs frame. Called as LED driver timer interrupt.
  * @param      None.
  * @retval     None.
  *****************************************************************************/
void led_driver_irq_handler(void);

/*******************************************************************************
  * @function   led_driver_set_brightness
  * @brief      Set PWM value.
  * @param      value: PWM value in [%].
  * @retval     None.
  *****************************************************************************/
void led_driver_set_brightness(uint8_t procent_val);
SYSCALL(led_driver_set_brightness, uint8_t)

/*******************************************************************************
  * @function   led_driver_overwrite_brightness
  * @brief      Overwrite PWM brightness.
  * @param      overwrite: overwrite brightness with @value or return to default
  *             behavior.
  * @param      value: PWM value to overwrite with in [%].
  * @retval     None.
  *****************************************************************************/
void led_driver_overwrite_brightness(bool overwrite, uint8_t value);
SYSCALL(led_driver_overwrite_brightness, bool, uint8_t)

/*******************************************************************************
  * @function   led_driver_get_brightness
  * @brief      Set PWM value.
  * @param      None.
  * @retval     PWM value in [%].
  *****************************************************************************/
uint8_t led_driver_get_brightness(void);

/*******************************************************************************
  * @function   led_driver_step_brightness
  * @brief      Decrease LED brightness by 10% each step (each function call).
  * @param      None.
  * @retval     None.
  *****************************************************************************/
void led_driver_step_brightness(void);

/*******************************************************************************
  * @function   led_set_user_mode
  * @brief      Set mode to LED(s) - default or user mode
  * @param      led: position of LED (0..11) or led >=12 -> all LED.
  * @parame     set: true to set user mode, false to unset
  * @retval     None.
  *****************************************************************************/
void led_set_user_mode(unsigned led, bool set);
SYSCALL(led_set_user_mode, unsigned, bool)

void led_states_commit(void);
void led_set_state_nocommit(unsigned led, bool state);

/*******************************************************************************
  * @function   led_set_state
  * @brief      Set state of the LED(s)
  * @param      led: position of LED (0..11) or led >=12 -> all LED.
  * @parame     state: false / true
  * @retval     None.
  *****************************************************************************/
void led_set_state(unsigned led, bool state);

/*******************************************************************************
  * @function   led_set_state_user
  * @brief      Set state of the LED(s) from user/I2C
  * @param      led: position of LED (0..11) or led >=12 -> all LED.
  * @parame     state: false / true
  * @retval     None.
  *****************************************************************************/
void led_set_state_user(unsigned led, bool state);
SYSCALL(led_set_state_user, unsigned, bool)

/*******************************************************************************
  * @function   led_driver_reset_pattern_start
  * @brief      Start knight rider pattern after reset.
  * @param      None.
  * @retval     None.
  *****************************************************************************/
void led_driver_reset_pattern_start(void);

/** Start blinking special pattern */
void led_driver_blink_pattern_start(uint16_t leds_mask, unsigned repeat,
				    unsigned timeout, unsigned last_timeout,
				    bool jump_to_after_reset_pattern);

/** Start bootloader blinking pattern with given color */
void led_driver_bootloader_pattern_start(uint32_t color);

#endif /*__LED_DRIVER_H */

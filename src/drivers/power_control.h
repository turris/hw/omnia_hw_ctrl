#ifndef POWER_CONTROL_H
#define POWER_CONTROL_H

#include "svc.h"

typedef enum {
	VOLTAGE_3V3 = 1,
	VOLTAGE_3V63 = 2,
	VOLTAGE_4V5 = 3,
	VOLTAGE_5V125 = 4,
} user_reg_voltage_t;

typedef enum {
	USB3_PORT0 = 0,
	USB3_PORT1 = 1
} usb_port_t;

/** Configure power control regulators and timer */
void power_control_config(void);

/** Start the board.
 * Set GPIO startup condition, disable voltage regulators, waits 100ms and
 * enable regulators.
 * @returns 0 on success, -n if enableing n-th regulator failed.
 */
int power_control_power_on(void);

/*******************************************************************************
  * @function   power_control_disable_regulators
  * @brief      Shutdown DC/DC regulators.
  * @param      None.
  * @retval     None.
  *****************************************************************************/
void power_control_disable_regulators(void);

/*******************************************************************************
  * @function   power_control_first_startup
  * @brief      Handle SYSRES_OUT, MAN_RES and CFG_CTRL signals during startup.
  * @param      None.
  * @retval     None.
  *****************************************************************************/
void power_control_first_startup(void);

/*******************************************************************************
  * @function   power_control_set_startup_condition
  * @brief      Set signals to reset state before board startup.
  * @param      None.
  * @retval     None.
  *****************************************************************************/
void power_control_set_startup_condition(void);

/** Enable or disable an USB port power.
 * @param port either USB3_PORT0 or USB3_PORT1
 * @param on true to enable the power, false to disable
 * @param autoenable_timeout timeout, in jiffies, when to automatically enable
 *        power to the USB port. Valid only if @ref on is false
 */
void power_control_usb(usb_port_t port, bool on, unsigned autoenable_timeout);
SYSCALL(power_control_usb, usb_port_t, bool, unsigned)

/** USB port autoenable handler. */
void power_control_usb_autoenable_handler(void);

/*******************************************************************************
  * @function   power_control_set_voltage
  * @brief      Set required voltage to the user regulator.
  * @param      voltage: enum value for desired voltage.
  * @retval     None.
  *****************************************************************************/
void power_control_set_voltage(user_reg_voltage_t voltage);

#endif /* !POWER_CONTROL_H */

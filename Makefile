CROSS_COMPILE	?= arm-none-eabi-

CC		= $(CROSS_COMPILE)gcc
CPP		= $(CROSS_COMPILE)gcc -E
OBJCOPY		= $(CROSS_COMPILE)objcopy
OBJDUMP		= $(CROSS_COMPILE)objdump

HOSTCC		= gcc
HOSTCFLAGS	= -O2

CFLAGS		= -ggdb -Os -fno-pie -ffreestanding -Wall -Wextra -Warray-bounds -nostdlib -ffunction-sections -fdata-sections
CPPFLAGS	= -DVERSION="{ $(shell git rev-parse HEAD | sed 's/\(..\)/0x\1, /g' | sed -r 's/,\s+$$//') }"
CPPFLAGS	+= -Isrc/include -Isrc/lib -Isrc/drivers -Isrc/application -Isrc/platform/common
CPPFLAGS_app	= -DBOOTLOADER_BUILD=0
CPPFLAGS_boot	= -DBOOTLOADER_BUILD=1

LDSCRIPT_app	= src/application/application.lds
LDSCRIPT_boot	= src/bootloader/bootloader.lds
LDFLAGS		= -T$(LDSCRIPT) -Wl,-Map=$(LDMAP),--cref -nostartfiles -no-pie -Xlinker --gc-sections -Wl,--nmagic

SRCS_DEBUG	= src/drivers/debug.c
SRCS_COMMON	= $(filter-out $(SRCS_DEBUG),$(wildcard src/drivers/*.c))
SRCS_COMMON	+= $(wildcard src/lib/*.c)
SRCS_COMMON	+= $(wildcard src/platform/common/*.c)
SRCS_app	= $(wildcard src/application/*.c)
SRCS_boot	= $(wildcard src/bootloader/*.c)

HOSTTOOLS = crc32tool stm32tool

ifeq ($(DBG_ENABLE), 1)
	SRCS_COMMON += $(SRCS_DEBUG)
	CPPFLAGS += -DDBG_ENABLE=1
	DEBUG_INFO_PRINT = \
		@echo -e "\n======================================================="; \
		echo "Built with debug output enabled on MCU's UART pins!"; \
		echo "MiniPCIe/mSATA card detection and PCIe1 PLED won't work"; \
		echo -e "=======================================================\n"
	ifdef DBG_BAUDRATE
		ifeq ($(shell echo "$(DBG_BAUDRATE)" | grep -qe "[^0-9]" && echo err),err)
			CPPFLAGS += $(error Wrong value for DBG_BAUDRATE: $(DBG_BAUDRATE))
		else
			CPPFLAGS += -DDBG_BAUDRATE=$(DBG_BAUDRATE)U
		endif
	else
		CPPFLAGS += -DDBG_BAUDRATE=115200U
	endif
else
	CPPFLAGS += -DDBG_ENABLE=0
	DEBUG_INFO_PRINT =
endif

ifeq ($(V), 1)
	Q =
	echo =
else
	Q = @
	echo = @printf "%26s %-4s [%-8s]  %s\n" "$(1)" "$(2)" "$(3)" "$(4)"
endif

.PHONY: all app boot clean

all: app boot tools/stm32tool

app:
	$(DEBUG_INFO_PRINT)
boot:
	$(DEBUG_INFO_PRINT)
clean:
	rm -rf $(addprefix tools/,$(HOSTTOOLS))

define HostToolRule
  tools/$(1): tools/$(1).c
	$(call echo,,,HOSTCC,$$<)
	$(Q)$(HOSTCC) $(HOSTCFLAGS) -o $$@ $$<
endef

$(foreach tool,$(HOSTTOOLS),$(eval $(call HostToolRule,$(tool))))

define CompileRule
  build.$(1)/$(2)/$(3)%.o: $(3)%.c
	$(call echo,$(1),$(2),CC,$$<)
	$(Q)mkdir -p $$(@D) && $$(CC) -c $$(CPPFLAGS) $$(CPPFLAGS_$(1)) $$(CFLAGS) $$(CFLAGS_$(1)) -MMD $$< -o $$@

  -include build.$(1)/$(2)/$(3)*.d
endef

define LinkScriptRule
  build.$(1)/$(2)/$(3): $(3)
	$(call echo,$(1),$(2),GENLDS,$$^)
	$(Q)mkdir -p $$(@D) && $$(CPP) $$(CPPFLAGS) $$(CPPFLAGS_$(1)) -D__ASSEMBLY__ -x assembler-with-cpp -std=c99 -MT build.$(1)/$(2)/$(3) -MMD -P -o $$@ $$<

  -include build.$(1)/$(2)/$(3:.lds=.d)
endef

define BinRule
ifeq ($(2),preboot)
  $(1).$(2).bin: build.$(1)/$(2).elf
	$(call echo,$(1),$(2),BIN,$$@)
	$(Q)$$(OBJCOPY) -O binary $$< $$@
else
ifneq ($$(SIGN_$(1)),)
  $(1).$(2).bin: build.$(1)/$(2).bin.crc tools/signing.py $(ECDSA_PRIV_KEY)
	$(call echo,$(1),$(2),SIGN,$$@)
ifeq ($(ECDSA_PRIV_KEY),)
	@echo -e "\n================================================================================"; \
	echo "The $(2) binary $$@ is being signed with development ECDSA key,"; \
	echo "and it is therefore built as a development build. In order to build a production"; \
	echo "binary, you must provide the production ECDSA private key with which it can be"; \
	echo "signed. The path to this key must be provided with the ECDSA_PRIV_KEY variable."; \
	echo -e "================================================================================\n"
endif
	$(Q)tools/signing.py sign $(if $(ECDSA_PRIV_KEY),$(ECDSA_PRIV_KEY),--development) $$< $$@
	$(Q)chmod +x $$@
endif

  $$(if $$(SIGN_$(1)),build.$(1)/$(2).bin.crc,$(1).$(2).bin): build.$(1)/$(2).bin.nocrc tools/crc32tool
	$(call echo,$(1),$(2),CRC32,$$@)
	$(Q)tools/crc32tool $(3) $(4) $$(if $$(SIGN_$(1)),96,0) $$< >$$@
	$(Q)chmod +x $$@

  build.$(1)/$(2).bin.nocrc: build.$(1)/$(2).elf
	$(call echo,$(1),$(2),BIN,$$@)
	$(Q)$$(OBJCOPY) -O binary $$< $$@
endif

  build.$(1)/$(2).elf: CPPFLAGS += $$(CPPFLAGS_$(2)) $$(CPPFLAGS_$(2)_$(1))
  build.$(1)/$(2).elf: LDSCRIPT = $$(LDSCRIPT_$(2)_$(1))
  build.$(1)/$(2).elf: LDMAP = build.$(1)/$(2).map
  build.$(1)/$(2).elf: $$(OBJS_$(2)_$(1)) $$(LDSCRIPT_$(2)_$(1))
	$(call echo,$(1),$(2),LD,$$@)
	$(Q)$$(CC) $$(CFLAGS) $$(CFLAGS_$(1)) $$(LDFLAGS) $$(OBJS_$(2)_$(1)) -o $$@
endef

define PlatBuildVariant
  SRCS_app_$(1) = $$(SRCS_COMMON) $$(SRCS_app) $$(SRCS_PLAT_$(1))
  SRCS_boot_$(1) = $$(SRCS_COMMON) $$(SRCS_boot) $$(SRCS_PLAT_$(1))
  OBJS_app_$(1) = $$(addprefix build.$(1)/app/,$$(SRCS_app_$(1):.c=.o))
  OBJS_boot_$(1) = $$(addprefix build.$(1)/boot/,$$(SRCS_boot_$(1):.c=.o))
  LDSCRIPT_app_$(1) = build.$(1)/app/$$(LDSCRIPT_app)
  LDSCRIPT_boot_$(1) = build.$(1)/boot/$$(LDSCRIPT_boot)

  PREBOOT_BIN_$(1) = $$(if $$(PREBOOT_$(1)),$(1).preboot.bin,)

  .PHONY: $(1) app_$(1) boot_$(1) clean_$(1)

  $(1): app_$(1) boot_$(1) $(1).flash.bin
  app_$(1): $(1).app.bin build.$(1)/app.dis
  boot_$(1): $(1).boot.bin build.$(1)/boot.dis

  $(1).flash.bin: $$(PREBOOT_BIN_$(1)) $(1).app.bin $(1).boot.bin tools/genflashimg.py
	$(call echo,$(1),,GENFLASH,$$@)
	$(Q)tools/genflashimg.py $$(FLASH_PAGE_SIZE_$(1)) $$(PREBOOT_BIN_$(1)) $(1).boot.bin $(1).app.bin $$(BOOT_MAX_SIZE_$(1)) $$@
	$(Q)chmod +x $$@

  clean_$(1):
	rm -rf build.$(1) $(1).flash.bin $(1).app.bin $(1).boot.bin $$(PREBOOT_BIN_$(1))

  build.$(1)/%.dis: build.$(1)/%.elf
	$(call echo,$(1),$$*,DISASM,$$@)
	$(Q)$$(OBJDUMP) -D -S $$< >$$@

  $$(eval $$(call BinRule,$(1),app,$$(CSUM_POS_$(1)),$$(FEAT_POS_$(1))))

  $$(eval $$(call LinkScriptRule,$(1),app,$$(LDSCRIPT_app)))

  $$(eval $$(call BinRule,$(1),boot,ignore,$$(BOOT_FEAT_POS_$(1))))

  $$(eval $$(call LinkScriptRule,$(1),boot,$$(LDSCRIPT_boot)))

  $$(foreach dir,$$(sort $$(dir $$(SRCS_app_$(1)))),$$(eval $$(call CompileRule,$(1),app,$$(dir))))
  $$(foreach dir,$$(sort $$(dir $$(SRCS_boot_$(1)))),$$(eval $$(call CompileRule,$(1),boot,$$(dir))))

  all: $(1)
  app: app_$(1)
  boot: boot_$(1)
  clean: clean_$(1)

ifneq ($$(PREBOOT_$(1)),)
  SRCS_preboot_$(1) = $$(wildcard $$(PREBOOT_$(1))/*.c)
  OBJS_preboot_$(1) = $$(addprefix build.$(1)/preboot/,$$(SRCS_preboot_$(1):.c=.o))
  LDSCRIPT_preboot_$(1) = build.$(1)/preboot/$$(PREBOOT_$(1))/preboot.lds

  $(1): preboot_$(1)

  preboot_$(1): $$(PREBOOT_BIN_$(1)) build.$(1)/preboot.dis

  $$(eval $$(call BinRule,$(1),preboot,,))

  $$(eval $$(call LinkScriptRule,$(1),preboot,$$(PREBOOT_$(1))/preboot.lds))

  $$(foreach dir,$$(sort $$(dir $$(SRCS_preboot_$(1)))),$$(eval $$(call CompileRule,$(1),preboot,$$(dir))))

  preboot: preboot_$(1)
endif
endef

define PlatDefVariant
  FLASH_PAGE_SIZE_$(1)-$(2) = $$(FLASH_PAGE_SIZE_$(1))
  BOOT_MAX_SIZE_$(1)-$(2) = $$(BOOT_MAX_SIZE_$(1))
  CSUM_POS_$(1)-$(2) = $$(CSUM_POS_$(1))
  FEAT_POS_$(1)-$(2) = $$(FEAT_POS_$(1))
  BOOT_FEAT_POS_$(1)-$(2) = $$(BOOT_FEAT_POS_$(1))
  SIGN_$(1)-$(2) := $$(SIGN_$(1))
  CFLAGS_$(1)-$(2) := $$(CFLAGS_$(1)) $$(CFLAGS_$(1)-$(2))
  CPPFLAGS_$(1)-$(2) := $$(CPPFLAGS_$(1)) $$(CPPFLAGS_$(1)-$(2))
  CPPFLAGS_app_$(1)-$(2) := $$(CPPFLAGS_app_$(1)) $$(CPPFLAGS_app_$(1)-$(2))
  CPPFLAGS_boot_$(1)-$(2) := $$(CPPFLAGS_boot_$(1)) $$(CPPFLAGS_boot_$(1)-$(2))
  SRCS_PLAT_$(1)-$(2) := $$(SRCS_PLAT_$(1)) $$(SRCS_PLAT_$(1)-$(2))
  PREBOOT_$(1)-$(2) := $$(PREBOOT_$(1))

  .PHONY: $(1)
  $(1): $(1)-$(2)
endef

define PlatBuild
  $$(foreach variant,$$(VARIANTS_$(1)), \
     $$(eval $$(call PlatDefVariant,$(1),$$(variant))) \
     $$(eval $$(call PlatBuildVariant,$(1)-$$(variant))) \
    )
endef

include Makedefs.*.mk

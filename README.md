# Turris Omnia Microcontroller Firmware

Microcontroller firmware for the Turris Omnia router.

This firmware takes care of:
* powering on / off voltage regulators in the correct order
* asserting / releasing reset to the SoC and peripherals
* handling front and rear button press events
* blinking RGB LEDs according to configuration
* handling I2C commands from the operating system
* providing true random numbers to the operating system
* providing board information (serial number, MAC addresses) and
  the ability to sign messages with board private key (on `mkl` boards)

See also the [`omnia-mcutool`](https://gitlab.nic.cz/turris/omnia-mcutool)
utility, which can be used to upgrade the firmware in the MCU and to send
various commands to it.

## Firmware variants

The Turris Omnia router can be shipped with 3 different MCUs:
STMicroelectronics' *STM32F030R8T6TR* (`stm32`), GigaDevice's
*GD32F150R8T6* (`gd32`) or NXP's *MKL81Z128VMC7* (`mkl`).

Moreover, there are various board revisions even for boards with
the same MCU, see the following table.

|                  |            board revision <= 23             | board revision >= 32 |
|:-----------------|:-------------------------------------------:|:--------------------:|
| `stm32` platform | `stm32-rev23`, `stm32-rev23-user-regulator` |     `stm32-rev32`    |
| `gd32` platform  |                `gd32-rev23`                 |     `gd32-rev32`     |
| `mkl` platform   |                                             |     `mkl-rev32`      |

You can use the `omnia-mcutool` utility mentioned above to determine which
variant of the firmware your board needs:

```
$ omnia-mcutool --firmware-version
...
Board firmware type: gd32-rev32
...
```

## Dependencies

To build the firmware, you will need the `arm-none-eabi` toolchain.
It is recommended to use the official [ARM GNU Toolchain](https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads).
Some Linux distributions also provide it in their repository
(e.g. the `gcc-arm-none-eabi` package on Debian / Ubuntu).

For the `mkl` build, you will also need *Python-3+* and the
[*ecdsa*](https://pypi.org/project/ecdsa/) package.

## Building

Use the following commands to download the ARM GNU Toolchain version `13.2Rel1`
and set it up in the `/tmp` directory (valid only for `x86-64` systems):

```
cd /tmp
wget https://developer.arm.com/-/media/Files/downloads/gnu/13.2.rel1/binrel/arm-gnu-toolchain-13.2.rel1-x86_64-arm-none-eabi.tar.xz
tar -xf arm-gnu-toolchain-13.2.rel1-x86_64-arm-none-eabi.tar.xz
rm arm-gnu-toolchain-13.2.rel1-x86_64-arm-none-eabi.tar.xz
export PATH=/tmp/arm-gnu-toolchain-13.2.Rel1-x86_64-arm-none-eabi/bin:"$PATH"
```

Then, clone the `omnia_hw_ctrl` repository and build it:

```
git clone https://gitlab.nic.cz/turris/hw/omnia_hw_ctrl
cd omnia_hw_ctrl
make -j$(nproc)
```

To build only a specific variant of the firmware, for example `gd32-rev32`,
type:

```
make -j$(nproc) gd32-rev32
```

### IMPORTANT NOTE FOR THE `mkl` PLATFORM

When building `mkl` firmware, the build system will print this:

```
================================================================================
The app binary mkl-rev32.app.bin is being signed with development ECDSA key,
and it is therefore built as a development build. In order to build a production
binary, you must provide the production ECDSA private key with which it can be
signed. The path to this key must be provided with the ECDSA_PRIV_KEY variable.
================================================================================
```

This is because Turris Omnia boards with the *MKL* microcontroller have board
cryptography provided by the MCU, instead of the Atmel SHA204A chip. This means
that there is a secret ECDSA private key stored inside the MCU, specific for the
board. The operating system can digitally sign messages with this private key
with the `omnia-mcutool` utility.

If a user could flash any firmware into the MCU, they would be able read this
secret private key (by editing the firmware so that it would report the private
key over I2C, for example). Because we want to prevent reading the private key,
the `mkl` firmware binaries need to be digitally signed by Turris. The firmware
upgrading code in the MCU will reject upgrading firmware that is not signed.

Therefore it is impossible for users to experiment with the MCU firmware on
`mkl` boards if they are using the `omnia-mcutool` utility to upgrade the
firmware.

The only way to flash non-Turris-signed firmware to `mkl` boards is via the
SWD debug connector CN14. A mass-erase operation must be executed on the MCU's
flash memory, which will erase the private key. Once the user flashes the new
firmware, a new private key will be generated.

## Flashing via `omnia-mcutool`

**[- WARNING: THIS CAN BRICK YOUR BOARD! -]**

For each variant the build process produces three binaries, ending in
`.boot.bin` (bootloader), `.app.bin` (application) and `.flash.bin` (bootloader
+ application).

In most scenarios you will only want to flash the application firmware. You can
do this by transferring the binary to you Turris Omnia board, and running the
following command in the oprating system:

```
omnia-mcutool -f VARIANT.app.bin
```

If everything goes smoothly, you can then reboot your board (`reboot`) to make
the MCU run the new firmware.

It is also possible to flash the bootloader firmware, with

```
omnia-mcutool --force -B -f VARIANT.boot.bin
```

## What to do if I managed to brick my board?

If you have flashed MCU firmware and your board does not do anything after you
power it up (no LED blinking), you have managed to brick it.

To repair it, you will need a SWD debugger adapter, capable of 3V3 signalling.

You will need to connect it to the connector C14 located between the
microcontroller and the ethernet switch chip, as show in the following
image:

![image](images/turris_omnia_mcu_debug_connector_location.jpg)

Because the connector pins are 1.27 mm apart, you will either need a
corresponding cable, or an adapter to 2.54 mm, such as this one:

![image](images/turris_omnia_mcu_debug_connector_adapter_1.27mm_to_2.54mm.jpg)
![image](images/turris_omnia_mcu_debug_connector_with_adapter.jpg)

You will need to connect signals `+3V3` (pin 1), `SWCLK` (pin 3),
`SWDIO` (pin 5) and `GND` (pin 9).

Download an official `VARIANT.flash.bin` binary for your board variant from
[releases](https://gitlab.nic.cz/turris/hw/omnia_hw_ctrl/-/releases).

Refer to your SWD debugger adapter documentation for information how to flash
the firmware binary into the microcontroller.

## License

The code is licensed under the [GNU GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.html).

For the STM32 platform, it uses files from *STM32F0xx Standard Peripherals Drivers* library,
which is licensed under the [MCD-ST Liberty SW License Agreement V2](http://www.st.com/software_license_agreement_liberty_v2).

For the GD32 platform, it uses header files from *GD32F1x0 Firmware Library*,
which is licensed under the [3-Clause BSD License](https://opensource.org/license/BSD-3-Clause).

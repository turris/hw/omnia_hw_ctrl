#!/usr/bin/env python
#
# Tool for signing Turris Omnia MCU firmware (used only on MKL MCUs)
#
# The tool supports two subcommands:
#   genkey - generates a new ECDSA key for signing firmware
#   sign - signes the firmware
#
# The elliptic curve used is the NIST256p curve.
#
# Singing appends a 96 bytes to the end of the file:
#   32 bytes: sha256 checksum of the file, used as message for ECDSA algorithm
#   32 bytes: R part of the ECDSA signature, little endian encoding
#   32 bytes: S part of the ECDSA signature, little endian encoding
#
# Example usage (from top-level of this repository):
#  signing.py sign path-to-ecdsa-private-key.txt \
#                  mkl-rev32.app.bin.no-signature \
#                  mkl-rev32.app.bin

import os, argparse
from sys import stderr
from ecdsa import NIST256p, SigningKey
from hashlib import sha256

def number_to_le(x, l):
	x = int(x)
	res = []
	for i in range(l):
		res.append(x & 0xff)
		x >>= 8
	return bytes(res)

def sigencode(r, s, order):
	l = (order.bit_length() + 7) // 8
	return number_to_le(r, l) + number_to_le(s, l)

def is_already_signed(data):
	# check whether the data is already signed by looking if it contains
	# sha256 checksum at position (filesize - 96)
	return sha256(data[:-96]).digest() == data[-96:-64]

parser = argparse.ArgumentParser(prog='signing.py')
subparsers = parser.add_subparsers(title='available subcommands', metavar='subcommand', dest='command', required=True)

parser_genkey = subparsers.add_parser('genkey',
				      description='''Generate a new ECDSA key pair for signing MCU firmware.
						     Print the public key as a C structure appropriate for
						     the firmware sources to stdout.''',
				      help='generate ECDSA key pair (the public key is printed as a C structure)')
parser_genkey.add_argument('--development', action='store_true', help='use development private key (testing vector from RFC 6979), do not generate a new random key')
parser_genkey.add_argument('ecdsa_priv_key', help='file where the ECDSA private key will be stored')

parser_sign = subparsers.add_parser('sign',
				    description='Sign a MCU firmware binary with an ECDSA private key.',
				    help='sign a firmware binary with ECDSA private key')
group = parser_sign.add_mutually_exclusive_group(required=True)
group.add_argument('--development', action='store_true', help='use development private key (testing vector from RFC 6979) for signing')
group.add_argument('ecdsa_priv_key', nargs='?', type=argparse.FileType('r'), help='file containing the ECDSA private key for signing')
parser_sign.add_argument('input', type=argparse.FileType('rb'), help='input firmware binary')
parser_sign.add_argument('output', help='where to store signed firmware binary')

args = parser.parse_args()

# from RFC 6979
DEV_PRIV_KEY = 0xc9afa9d845ba75166b5c215767b1d6934e50c3db36e89b127b8a622b120f6721

if args.command == 'genkey':
	if args.development:
		sk = SigningKey.from_secret_exponent(DEV_PRIV_KEY, curve=NIST256p)
	else:
		sk = SigningKey.generate(curve=NIST256p)

	secexp = sk.privkey.secret_multiplier

	def priv_opener(path, flags):
		return os.open(path, flags, mode=0o600)

	with open(args.ecdsa_priv_key, 'w', opener=priv_opener) as f:
		print(secexp, file=f)

	if not args.development:
		print(f'private key stored to {args.ecdsa_priv_key}\n', file=stderr)

	print('printing the structures of public key that can be incorporated into the code\n', file=stderr)

	def to_le32_array(n, x):
		xs = []
		for i in range(8):
			xs.append(f'0x{x & 0xffffffff:08x}')
			x >>= 32
		print(f'\t.{n} = {{ .words = {{ {xs[0]}, {xs[1]}, {xs[2]}, {xs[3]},')
		print(f'\t\t\t  {xs[4]}, {xs[5]}, {xs[6]}, {xs[7]} }}}},')

	print('static const pkha_point_t fw_signing_pub_key = {')
	to_le32_array('x', int(sk.privkey.public_key.point.x()))
	to_le32_array('y', int(sk.privkey.public_key.point.y()))
	print('};')
elif args.command == 'sign':
	if args.development:
		secexp = DEV_PRIV_KEY
	else:
		secexp = int(args.ecdsa_priv_key.readline().strip())

	sk = SigningKey.from_secret_exponent(secexp, curve=NIST256p, hashfunc=sha256)

	data = args.input.read()

	if is_already_signed(data):
		print('Output file {args.output} seems already signed.', file=stderr)
		exit(1)

	digest = sha256(data).digest()
	signature = sk.sign_deterministic(data, sigencode=sigencode)

	open(args.output, 'wb').write(data + digest + signature)

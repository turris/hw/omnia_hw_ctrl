#!/usr/bin/env python
#
# Tool for generating whole flash image for Turris Omnia MCU firmware

import os, argparse
from sys import stderr

def sector_size(name, mul):
	def convert(val):
		try:
			ival = int(val, 0)
			if ival < mul or ival % mul:
				raise
		except:
			raise argparse.ArgumentTypeError(f'invalid {name}: \'{val}\'')

		return ival

	return convert

parser = argparse.ArgumentParser(prog='genflashimg.py')
parser.add_argument('page_size', metavar='flash_page_size', type=sector_size('flash page size', 0x400), help='flash page size')
parser.add_argument('preboot', metavar='preboot.bin', nargs='?', type=argparse.FileType('rb'), help='prebootloader binary')
parser.add_argument('boot', metavar='boot.bin', type=argparse.FileType('rb'), help='bootloader binary')
parser.add_argument('app', metavar='app.bin', type=argparse.FileType('rb'), help='application binary')
parser.add_argument('boot_size', metavar='boot_partition_size', type=sector_size('boot partition size', 0x400), help='boot partition size')
parser.add_argument('flash', metavar='flash.bin', help='output MCU firmware binary')

args = parser.parse_args()

if args.boot_size % args.page_size:
	print('genflashimg.py: error: boot partition size not a multiple of flash page size', file=stderr)
	exit(1)

if args.preboot:
	preboot_buf = args.preboot.read()
	if len(preboot_buf) > args.page_size:
		print(f'genflashimg.py: error: preboot firmware file {args.preboot.name} too large', file=stderr)
		exit(1)

	# append 0xFF bytes to preboot firmware until it is page_size large
	preboot_buf += b'\xff' * (args.page_size - len(preboot_buf))
else:
	preboot_buf = b''

boot_buf = args.boot.read()
if len(boot_buf) > args.boot_size:
	print(f'genflashimg.py: error: boot firmware file {args.boot.name} too large', file=stderr)
	exit(1)

# append 0xFF bytes to boot firmware until it is boot_size large
boot_buf += b'\xff' * (args.boot_size - len(boot_buf))

app_buf = args.app.read()

# append 0xFF bytes until app firmware aligned to page_size bytes
if len(app_buf) % args.page_size:
	app_buf += b'\xff' * (args.page_size - len(app_buf) % args.page_size)

# write flash firmware binary
open(args.flash, 'wb').write(preboot_buf + boot_buf + app_buf)

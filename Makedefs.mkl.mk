SRCS_PLAT_mkl	= $(wildcard src/platform/mkl/*.c)

FLASH_PAGE_SIZE_mkl	= 0x800
ISR_VECTOR_LENGTH_mkl	= 0xC0
BOOT_MAX_SIZE_mkl	= 0x7800
CSUM_POS_mkl		= $(ISR_VECTOR_LENGTH_mkl)
FEAT_POS_mkl		= 0xC8
BOOT_FEAT_POS_mkl	= 0xD4
PREBOOT_mkl		= src/platform/mkl/preboot
SIGN_mkl		= true

CPPFLAGS_mkl	= -DMKL81 -DMCU_TYPE=MKL
CPPFLAGS_mkl	+= -DSYS_CORE_FREQ=96000000U -DFLASH_PAGE_SIZE=$(FLASH_PAGE_SIZE_mkl)
CPPFLAGS_mkl	+= -DBOOTLOADER_MAX_SIZE=$(BOOT_MAX_SIZE_mkl) -DISR_VECTOR_LENGTH=$(ISR_VECTOR_LENGTH_mkl)
CPPFLAGS_mkl	+= -DPRIVILEGES=1 -DTRNG_ENABLED=1 -DCRYPTO_ENABLED=1 -DBOARD_INFO_ENABLED=1
CPPFLAGS_mkl	+= -Isrc/platform/mkl
CPPFLAGS_mkl	+= -DPOWEROFF_WAKEUP_ENABLED=1

# forbid flashing bootloader firmware in application
CPPFLAGS_boot_mkl	= -DFORBID_FIRMWARE_FLASH=0
CPPFLAGS_app_mkl	= -DFORBID_FIRMWARE_FLASH=1

CFLAGS_mkl	= -mcpu=cortex-m0plus -mthumb -mlittle-endian -masm-syntax-unified

# since 0 is a valid address, we need this to avoid array-bounds warnings and null pointer dereference traps
CFLAGS_mkl	+= --param=min-pagesize=0 -fno-delete-null-pointer-checks

VARIANTS_mkl = rev32
CPPFLAGS_mkl-rev32 = -DOMNIA_BOARD_REVISION=32 -DUSER_REGULATOR_ENABLED=0

ifeq ($(TRNG_TEST_HW_ERRORS), 1)
	CPPFLAGS_mkl += -DTRNG_TEST_HW_ERRORS=1
else
	CPPFLAGS_mkl += -DTRNG_TEST_HW_ERRORS=0
endif

ifeq ($(CRYPTO_TEST), 1)
	CPPFLAGS_mkl += -DCRYPTO_TEST=1
else
	CPPFLAGS_mkl += -DCRYPTO_TEST=0
endif

ifeq ($(ECDSA_PRIV_KEY),)
	CPPFLAGS_mkl += -DPRODUCTION_BUILD=0
else
	CPPFLAGS_mkl += -DPRODUCTION_BUILD=1
endif

$(eval $(call PlatBuild,mkl))
